function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-start-start-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/start/start.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/start/start.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesStartStartPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content class=\"ion-padding bg-image\" scrollX=false>\n  <div class=\"main-content\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <img [src]=\"imgMBPJ\">\n        </ion-col>\n        <ion-col>\n          <img [src]=\"imgSmartPJ\">\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <div class=\"login-title\">\n      <h2>Skim Rebat Cukai Taksiran Rumah Mesra Alam Hijau Petaling Jaya</h2>\n    </div>\n  </div>\n</ion-content>\n\n<ion-footer class=\"ion-padding ion-no-border\" mode=\"ios\">\n\t<ion-grid>\n\t\t<ion-row>\n\t\t\t<ion-col>\n        <p class=\"copyright\">Hak cipta terpelihara Majlis Bandaraya Petaling Jaya</p>\n\t\t\t\t<ion-button\n\t\t\t\t\tshape=\"round\"\n\t\t\t\t\texpand=\"block\"\n          (click)=\"navigatePage('/auth/walkthrough')\"\n          mode=\"ios\"\n\t\t\t\t>\n\t\t\t\t\tStart\n\t\t\t\t</ion-button>\n\t\t\t</ion-col>\n\t\t</ion-row>\n\t</ion-grid>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pages/start/start-routing.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/start/start-routing.module.ts ***!
    \*****************************************************/

  /*! exports provided: StartPageRoutingModule */

  /***/
  function srcAppPagesStartStartRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StartPageRoutingModule", function () {
      return StartPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _start_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./start.page */
    "./src/app/pages/start/start.page.ts");

    var routes = [{
      path: '',
      component: _start_page__WEBPACK_IMPORTED_MODULE_3__["StartPage"]
    }];

    var StartPageRoutingModule = function StartPageRoutingModule() {
      _classCallCheck(this, StartPageRoutingModule);
    };

    StartPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], StartPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/start/start.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/pages/start/start.module.ts ***!
    \*********************************************/

  /*! exports provided: StartPageModule */

  /***/
  function srcAppPagesStartStartModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StartPageModule", function () {
      return StartPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _start_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./start-routing.module */
    "./src/app/pages/start/start-routing.module.ts");
    /* harmony import */


    var _start_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./start.page */
    "./src/app/pages/start/start.page.ts");

    var StartPageModule = function StartPageModule() {
      _classCallCheck(this, StartPageModule);
    };

    StartPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _start_routing_module__WEBPACK_IMPORTED_MODULE_5__["StartPageRoutingModule"]],
      declarations: [_start_page__WEBPACK_IMPORTED_MODULE_6__["StartPage"]]
    })], StartPageModule);
    /***/
  },

  /***/
  "./src/app/pages/start/start.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/pages/start/start.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesStartStartPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host {\n  background: #fff no-repeat center/cover url('landing-tower.jpg');\n  text-align: center;\n  -ms-background-size: cover;\n  background-size: cover;\n}\n\nion-content {\n  --padding-end: 24px;\n  --padding-start: 24px;\n  --overflow: hidden;\n  --background: transparent;\n}\n\nion-footer {\n  background: transparent;\n}\n\n.main-content {\n  display: grid;\n  align-items: center;\n  align-content: center;\n  justify-content: center;\n  justify-items: center;\n  height: 100vh;\n}\n\nimg {\n  max-width: 8rem;\n}\n\n.login-title {\n  margin-top: 0.5vh;\n  margin-bottom: 2vh;\n}\n\n.login-title h2 {\n  font-size: 1.5em;\n  color: black;\n}\n\n.copyright {\n  font-size: 0.8rem;\n  font-weight: 500;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9tb2JpbGUvc3JjL2FwcC9wYWdlcy9zdGFydC9zdGFydC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3N0YXJ0L3N0YXJ0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdFQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUlBLHNCQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7QUNDSjs7QURFQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGFBQUE7QUNDSjs7QURHQTtFQUNJLGVBQUE7QUNBSjs7QURHQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNBSjs7QURDSTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtBQ0NSOztBREdBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3N0YXJ0L3N0YXJ0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmIG5vLXJlcGVhdCBjZW50ZXIvY292ZXIgdXJsKCdzcmMvYXNzZXRzL2ltZy9iYWNrZ3JvdW5kL2xhbmRpbmctdG93ZXIuanBnJyk7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIC1tcy1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG5pb24tY29udGVudCB7XG4gICAgLS1wYWRkaW5nLWVuZDogMjRweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDI0cHg7XG4gICAgLS1vdmVyZmxvdzogaGlkZGVuO1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG4gIFxuaW9uLWZvb3RlciB7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbi5tYWluLWNvbnRlbnQge1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAganVzdGlmeS1pdGVtczogY2VudGVyO1xuICAgIGhlaWdodDogMTAwdmg7XG59XG5cblxuaW1nIHtcbiAgICBtYXgtd2lkdGg6IDhyZW07XG59XG5cbi5sb2dpbi10aXRsZSB7XG4gICAgbWFyZ2luLXRvcDogMC41dmg7XG4gICAgbWFyZ2luLWJvdHRvbTogMnZoO1xuICAgIGgyIHtcbiAgICAgICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICAgICAgY29sb3I6IGJsYWNrOyAvLyB3aGl0ZTtcbiAgICB9XG59XG5cbi5jb3B5cmlnaHQge1xuICAgIGZvbnQtc2l6ZTogMC44cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgY29sb3I6IGJsYWNrOyAvLyB3aGl0ZTtcbn0iLCI6aG9zdCB7XG4gIGJhY2tncm91bmQ6ICNmZmYgbm8tcmVwZWF0IGNlbnRlci9jb3ZlciB1cmwoXCJzcmMvYXNzZXRzL2ltZy9iYWNrZ3JvdW5kL2xhbmRpbmctdG93ZXIuanBnXCIpO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIC1tcy1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAtby1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAtbW96LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLXBhZGRpbmctZW5kOiAyNHB4O1xuICAtLXBhZGRpbmctc3RhcnQ6IDI0cHg7XG4gIC0tb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4ubWFpbi1jb250ZW50IHtcbiAgZGlzcGxheTogZ3JpZDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAganVzdGlmeS1pdGVtczogY2VudGVyO1xuICBoZWlnaHQ6IDEwMHZoO1xufVxuXG5pbWcge1xuICBtYXgtd2lkdGg6IDhyZW07XG59XG5cbi5sb2dpbi10aXRsZSB7XG4gIG1hcmdpbi10b3A6IDAuNXZoO1xuICBtYXJnaW4tYm90dG9tOiAydmg7XG59XG4ubG9naW4tdGl0bGUgaDIge1xuICBmb250LXNpemU6IDEuNWVtO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5jb3B5cmlnaHQge1xuICBmb250LXNpemU6IDAuOHJlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgY29sb3I6IGJsYWNrO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/start/start.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/pages/start/start.page.ts ***!
    \*******************************************/

  /*! exports provided: StartPage */

  /***/
  function srcAppPagesStartStartPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StartPage", function () {
      return StartPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var StartPage = /*#__PURE__*/function () {
      function StartPage(router) {
        _classCallCheck(this, StartPage);

        this.router = router; // Image

        this.imgMBPJ = 'assets/img/organization/mbpj-logo.png';
        this.imgSmartPJ = 'assets/img/organization/smart-pj.png';
      }

      _createClass(StartPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "navigatePage",
        value: function navigatePage(path) {
          var navigationPath = path;
          this.router.navigate([navigationPath]);
        }
      }]);

      return StartPage;
    }();

    StartPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    StartPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-start',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./start.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/start/start.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./start.page.scss */
      "./src/app/pages/start/start.page.scss"))["default"]]
    })], StartPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-start-start-module-es5.js.map