(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-information-information-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/information/information.page.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/information/information.page.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n\t<ion-toolbar mode=\"ios\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button \n\t\t\t\tdefaultHref=\"/core/profile\"\n\t\t\t\trouterLink=\"/core/profile\"\n        routerDirection=\"back\"\n        text=\"\"\n\t\t\t>\n\t\t\t</ion-back-button>\n    </ion-buttons>\n    <ion-title>Information</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list *ngIf=\"!isEdit\" class=\"ion-no-margin list-core\" mode=\"ios\">\n    <ion-item class=\"item-form\" lines=\"none\">\n      <ion-label position=\"stacked\" mode=\"ios\">Full Name</ion-label>\n      <ion-input *ngIf=\"user\" value=\"{{user.full_name}}\" disabled mode=\"ios\"></ion-input>\n      <ion-input *ngIf=\"!user\" value=\"\" disabled mode=\"ios\"></ion-input>\n    </ion-item>\n\n    <ion-item  class=\"item-form\" lines=\"none\">\n      <ion-label position=\"stacked\" mode=\"ios\">New NRIC</ion-label>\n      <ion-input *ngIf=\"user\" value=\"{{user.nric_new}}\" disabled mode=\"ios\"></ion-input>\n      <ion-input *ngIf=\"!user\" value=\"\" disabled mode=\"ios\"></ion-input>\n    </ion-item>\n\n    <ion-item  class=\"item-form\" lines=\"none\">\n      <ion-label position=\"stacked\" mode=\"ios\">Old NRIC</ion-label>\n      <ion-input *ngIf=\"user\" value=\"{{user.nric_old}}\" disabled mode=\"ios\"></ion-input>\n      <ion-input *ngIf=\"!user\" value=\"\" disabled mode=\"ios\"></ion-input>\n    </ion-item>\n\n    <ion-item  class=\"item-form\" lines=\"none\">\n      <ion-label position=\"stacked\" mode=\"ios\">Email</ion-label>\n      <ion-input *ngIf=\"user\" value=\"{{user.email}}\" disabled mode=\"ios\"></ion-input>\n      <ion-input *ngIf=\"!user\" value=\"\" disabled mode=\"ios\"></ion-input>\n    </ion-item>\n\n    <ion-item  class=\"item-form\" lines=\"none\">\n      <ion-label position=\"stacked\" mode=\"ios\">Mobile</ion-label>\n      <ion-input *ngIf=\"user\" value=\"{{user.nric_old}}\" disabled mode=\"ios\"></ion-input>\n      <ion-input *ngIf=\"!user\" value=\"\" disabled mode=\"ios\"></ion-input>\n    </ion-item>\n\n    <ion-item  class=\"item-form\" lines=\"none\">\n      <ion-label position=\"stacked\" mode=\"ios\">Phone</ion-label>\n      <ion-input *ngIf=\"user\" value=\"{{user.nric_old}}\" disabled mode=\"ios\"></ion-input>\n      <ion-input *ngIf=\"!user\" value=\"\" disabled mode=\"ios\"></ion-input>\n    </ion-item>\n\n    <ion-item  class=\"item-form\" lines=\"none\">\n      <ion-label position=\"stacked\" mode=\"ios\">Occupation</ion-label>\n      <ion-input *ngIf=\"user\" value=\"{{user.nric_old}}\" disabled mode=\"ios\"></ion-input>\n      <ion-input *ngIf=\"!user\" value=\"\" disabled mode=\"ios\"></ion-input>\n    </ion-item>\n\n    <ion-item  class=\"item-form\" lines=\"none\">\n      <ion-label position=\"stacked\" mode=\"ios\">Gender</ion-label>\n      <ion-input *ngIf=\"user\" value=\"{{user.nric_old}}\" disabled mode=\"ios\"></ion-input>\n      <ion-input *ngIf=\"!user\" value=\"\" disabled mode=\"ios\"></ion-input>\n    </ion-item>\n  </ion-list>\n\n  <ion-list *ngIf=\"isEdit\" class=\"ion-no-margin list-core\" mode=\"ios\">\n    <form [formGroup]=\"userForm\">\n      <ion-item class=\"item-form\" lines=\"none\">\n        <ion-label position=\"stacked\" mode=\"ios\">Full Name</ion-label>\n        <ion-input mode=\"ios\" formControlName=\"full_name\" type=\"text\"></ion-input>\n      </ion-item>\n  \n      <ion-item  class=\"item-form\" lines=\"none\">\n        <ion-label position=\"stacked\" mode=\"ios\">New NRIC / Passport</ion-label>\n        <ion-input value=\"{{user.nric_new}}\" mode=\"ios\" disabled></ion-input>\n      </ion-item>\n  \n      <ion-item  class=\"item-form\" lines=\"none\">\n        <ion-label position=\"stacked\" mode=\"ios\">Old NRIC</ion-label>\n        <ion-input placeholder=\"A271281\" mode=\"ios\" formControlName=\"nric_old\" type=\"text\"></ion-input>\n      </ion-item>\n\n      <ion-item  class=\"item-form\" lines=\"none\">\n        <ion-label position=\"stacked\" mode=\"ios\">Email</ion-label>\n        <ion-input placeholder=\"ali@gmail.com\" mode=\"ios\" formControlName=\"email\" type=\"email\"></ion-input>\n      </ion-item>\n  \n      <ion-item  class=\"item-form\" lines=\"none\">\n        <ion-label position=\"stacked\" mode=\"ios\">Mobile</ion-label>\n        <ion-input placeholder=\"0129121231\" mode=\"ios\" formControlName=\"mobile\" type=\"number\"></ion-input>\n      </ion-item>\n  \n      <ion-item  class=\"item-form\" lines=\"none\">\n        <ion-label position=\"stacked\" mode=\"ios\">Phone</ion-label>\n        <ion-input placeholder=\"0371273173\" mode=\"ios\" formControlName=\"phone\" type=\"number\"></ion-input>\n      </ion-item>\n  \n      <ion-item  class=\"item-form\" lines=\"none\">\n        <ion-label position=\"stacked\" mode=\"ios\">Occupation</ion-label>\n        <ion-input placeholder=\"Teacher\" mode=\"ios\" formControlName=\"occupation\" type=\"text\"></ion-input>\n      </ion-item>\n  \n      <ion-item  class=\"item-form\" lines=\"none\">\n        <ion-label position=\"stacked\" mode=\"ios\">Gender</ion-label>\n        <ion-select \n          mode=\"ios\"\n          formControlName=\"gender\"\n          placeholder=\"Male\"\n        >\n          <ion-select-option value=\"FM\">Female</ion-select-option>\n          <ion-select-option value=\"ML\">Male</ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <ion-item  class=\"item-form\" lines=\"none\">\n        <ion-label position=\"stacked\" mode=\"ios\">NRIC Photo</ion-label>\n        <ion-button mode=\"ios\" color=\"primary\" expand=\"block\" (click)=\"openUploadSheet()\">Upload</ion-button>\n      </ion-item>\n    </form>\n    <ion-item class=\"item-form\" lines=\"none\" *ngIf=\"imageSelectedPreview\">\n      <ion-img [src]=\"imageSelectedPreview\"></ion-img>\n    </ion-item>\n  </ion-list>\n</ion-content>\n\n<ion-footer class=\"ion-no-border ion-padding\" mode=\"ios\">\n  <ion-button \n    *ngIf=\"!isEdit\"\n    expand=\"block\"\n    mode=\"ios\"\n    shape=\"round\"\n    color=\"secondary\"\n    (click)=\"edit()\"\n  >\n    Edit\n  </ion-button>\n  <ion-button \n    *ngIf=\"isEdit && !isSaving\"\n    expand=\"block\"\n    mode=\"ios\"\n    shape=\"round\"\n    color=\"primary\"\n    [disabled]=\"!userForm.valid\"\n    (click)=\"save()\"\n  >\n    Save\n  </ion-button>\n  <ion-button \n    *ngIf=\"isEdit && isSaving\"\n    expand=\"block\"\n    mode=\"ios\"\n    shape=\"round\"\n    color=\"primary\"\n    disabled\n  >\n    <ion-spinner name=\"bubbles\"></ion-spinner>\n  </ion-button>\n</ion-footer>");

/***/ }),

/***/ "./src/app/core/information/information-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/core/information/information-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: InformationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InformationPageRoutingModule", function() { return InformationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _information_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./information.page */ "./src/app/core/information/information.page.ts");




const routes = [
    {
        path: '',
        component: _information_page__WEBPACK_IMPORTED_MODULE_3__["InformationPage"]
    }
];
let InformationPageRoutingModule = class InformationPageRoutingModule {
};
InformationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], InformationPageRoutingModule);



/***/ }),

/***/ "./src/app/core/information/information.module.ts":
/*!********************************************************!*\
  !*** ./src/app/core/information/information.module.ts ***!
  \********************************************************/
/*! exports provided: InformationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InformationPageModule", function() { return InformationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _information_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./information-routing.module */ "./src/app/core/information/information-routing.module.ts");
/* harmony import */ var _information_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./information.page */ "./src/app/core/information/information.page.ts");







let InformationPageModule = class InformationPageModule {
};
InformationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _information_routing_module__WEBPACK_IMPORTED_MODULE_5__["InformationPageRoutingModule"]
        ],
        declarations: [_information_page__WEBPACK_IMPORTED_MODULE_6__["InformationPage"]]
    })
], InformationPageModule);



/***/ }),

/***/ "./src/app/core/information/information.page.scss":
/*!********************************************************!*\
  !*** ./src/app/core/information/information.page.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvaW5mb3JtYXRpb24vaW5mb3JtYXRpb24ucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/core/information/information.page.ts":
/*!******************************************************!*\
  !*** ./src/app/core/information/information.page.ts ***!
  \******************************************************/
/*! exports provided: InformationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InformationPage", function() { return InformationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_shared_services_users_users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/users/users.service */ "./src/app/shared/services/users/users.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/auth/auth.service */ "./src/app/shared/services/auth/auth.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");







let InformationPage = class InformationPage {
    constructor(authService, userService, fb, actionSheetCtrl, camera) {
        this.authService = authService;
        this.userService = userService;
        this.fb = fb;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        // Checker
        this.isEdit = false;
        this.isSaving = false;
        this.getData();
    }
    ngOnInit() {
        this.userForm = this.fb.group({
            full_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.user.full_name, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.user.email),
            nric_old: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.user.nric_old),
            nric_new: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.user.nric_new, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            nric_doc: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            mobile: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.user.mobile, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.user.phone, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            occupation: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.user.occupation),
            gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.user.gender)
        });
    }
    getData() {
        this.user = this.userService.userCurrent;
        this.userTemp = this.userService.userCurrent;
        if (this.userTemp.gender == 'ML') {
            this.userTemp.gender = 'Male';
        }
        else if (this.userTemp.gender == 'FM') {
            this.userTemp.gender = 'Female';
        }
        else if (this.userTemp.gender == 'NA') {
            this.userTemp.gender = 'Not Available';
        }
    }
    edit() {
        this.isEdit = true;
    }
    save() {
        this.isSaving = true;
        if (!this.userForm.value.full_name) {
            this.userForm.controls['full_name'].setValue(this.user.full_name);
        }
        if (!this.userForm.value.email) {
            this.userForm.controls['email'].setValue(this.user.email);
        }
        if (!this.userForm.value.nric_old) {
            this.userForm.controls['nric_old'].setValue(this.user.nric_old);
        }
        if (!this.userForm.value.nric_new) {
            this.userForm.controls['nric_new'].setValue(this.user.nric_new);
        }
        if (!this.userForm.value.mobile) {
            this.userForm.controls['mobile'].setValue(this.user.mobile);
        }
        if (!this.userForm.value.phone) {
            this.userForm.controls['phone'].setValue(this.user.phone);
        }
        if (!this.userForm.value.occupation) {
            this.userForm.controls['occupation'].setValue(this.user.occupation);
        }
        if (!this.userForm.value.gender) {
            this.userForm.controls['gender'].setValue(this.user.gender);
        }
        this.submit();
    }
    submit() {
        console.log(this.userForm.value);
        this.userService.update(this.user.id, this.userForm.value).subscribe(() => { }, () => { }, () => {
            this.isSaving = false;
        });
    }
    openUploadSheet() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetCtrl.create({
                header: 'Upload NRIC / passport',
                // cssClass: 'my-custom-class',
                buttons: [
                    {
                        text: 'Gallery',
                        icon: 'images-outline',
                        handler: () => {
                            this.openGallery();
                        }
                    },
                    {
                        text: 'Camera',
                        icon: 'camera-outline',
                        handler: () => {
                            this.openCamera();
                        }
                    },
                    {
                        text: 'Cancel',
                        icon: 'close-outline',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    openGallery() {
        // console.log('Gallery opened')
        let cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 60,
            targetWidth: 1000,
            targetHeight: 1000,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true
        };
        this.camera.getPicture(cameraOptions)
            .then((file_uri) => {
            this.imageSelected = 'data:image/jpeg;base64,' + file_uri;
            this.imageSelectedPreview = 'data:image/jpeg;base64,' + file_uri;
            this.userForm.controls['nric_doc'].setValue(this.imageSelected);
            console.log('control: ', this.userForm.value.nric_doc);
            console.log('imgFile', this.imageSelected);
            // this.imageSelectedPreview = 'data:image/jpeg;base64,' + this.imageSelected
            // this.houseForm.controls['assessment_tax_doc'].setValue(this.imageSelectedPreview)
        }, (err) => {
            console.log(err);
        });
    }
    openCamera() {
        // console.log('Camera opened')
        const options = {
            quality: 60,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.camera.getPicture(options).then((imageData) => {
            this.imageSelected = imageData;
            this.imageSelectedPreview = window.Ionic.WebView.convertFileSrc(this.imageSelected);
            this.userForm.controls['nric_doc'].setValue(this.imageSelected);
        }, (err) => {
            alert("error " + JSON.stringify(err));
        });
    }
};
InformationPage.ctorParameters = () => [
    { type: src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_shared_services_users_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ActionSheetController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"] }
];
InformationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-information',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./information.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/information/information.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./information.page.scss */ "./src/app/core/information/information.page.scss")).default]
    })
], InformationPage);



/***/ })

}]);
//# sourceMappingURL=core-information-information-module-es2015.js.map