(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-applications-applications-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/applications/applications.page.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/applications/applications.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Applications</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen>\n  <ion-list mode=\"ios\" class=\"ion-no-margin list-core\">\n    <ion-list-header mode=\"ios\">\n      <h3>Applications List</h3>\n    </ion-list-header>\n\n    <ion-card mode=\"ios\" [hidden]=\"isApplicationsEmpty\">\n      <ion-card-content class=\"ion-no-padding ion-padding-start ion-padding-end\">\n        <ion-item class=\"ion-no-padding\" lines=\"none\">\n          <ion-icon slot=\"start\" name=\"document-text-outline\" color=\"primary\"></ion-icon>\n          <ion-label class=\"ion-text-wrap\">\n            <h2>14/4/2020</h2>\n            <p>No. 25, Lorong Pablo Escobar</p>\n        </ion-label>\n        <ion-chip color=\"success\">\n          <ion-label>Approved</ion-label>\n        </ion-chip>\n        </ion-item>\n      </ion-card-content>\n      <ion-card-header color=\"success\" class=\"ion-no-padding ion-no-margin\">\n        <ion-button expand=\"full\" class=\"ion-no-margin\">Click to view</ion-button>\n      </ion-card-header>\n    </ion-card>\n\n    <div [hidden]=\"!isApplicationsEmpty\">\n      <img [src]=\"iconError\" class=\"error-img\">\n      <p class=\"error-text\">No application submitted yet</p>\n    </div>\n  </ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/core/applications/applications-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/applications/applications-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: ApplicationsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationsPageRoutingModule", function() { return ApplicationsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _applications_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./applications.page */ "./src/app/core/applications/applications.page.ts");




const routes = [
    {
        path: '',
        component: _applications_page__WEBPACK_IMPORTED_MODULE_3__["ApplicationsPage"]
    }
];
let ApplicationsPageRoutingModule = class ApplicationsPageRoutingModule {
};
ApplicationsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ApplicationsPageRoutingModule);



/***/ }),

/***/ "./src/app/core/applications/applications.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/core/applications/applications.module.ts ***!
  \**********************************************************/
/*! exports provided: ApplicationsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationsPageModule", function() { return ApplicationsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _applications_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./applications-routing.module */ "./src/app/core/applications/applications-routing.module.ts");
/* harmony import */ var _applications_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./applications.page */ "./src/app/core/applications/applications.page.ts");







let ApplicationsPageModule = class ApplicationsPageModule {
};
ApplicationsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _applications_routing_module__WEBPACK_IMPORTED_MODULE_5__["ApplicationsPageRoutingModule"]
        ],
        declarations: [_applications_page__WEBPACK_IMPORTED_MODULE_6__["ApplicationsPage"]]
    })
], ApplicationsPageModule);



/***/ }),

/***/ "./src/app/core/applications/applications.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/core/applications/applications.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error-img {\n  display: grid;\n  max-width: 7rem !important;\n  margin-top: 5rem;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.error-text {\n  color: #9d9fa6;\n  display: grid;\n  font-size: 0.9rem;\n  margin-left: 4rem;\n  margin-right: 4rem;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9tb2JpbGUvc3JjL2FwcC9jb3JlL2FwcGxpY2F0aW9ucy9hcHBsaWNhdGlvbnMucGFnZS5zY3NzIiwic3JjL2FwcC9jb3JlL2FwcGxpY2F0aW9ucy9hcHBsaWNhdGlvbnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxjQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb3JlL2FwcGxpY2F0aW9ucy9hcHBsaWNhdGlvbnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yLWltZyB7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBtYXgtd2lkdGg6IDdyZW0gIWltcG9ydGFudDtcbiAgICBtYXJnaW4tdG9wOiA1cmVtO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbn1cbiAgXG4uZXJyb3ItdGV4dCB7XG4gICAgY29sb3I6ICM5ZDlmYTY7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBmb250LXNpemU6IDAuOXJlbTtcbiAgICBtYXJnaW4tbGVmdDogNHJlbTtcbiAgICBtYXJnaW4tcmlnaHQ6IDRyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufSIsIi5lcnJvci1pbWcge1xuICBkaXNwbGF5OiBncmlkO1xuICBtYXgtd2lkdGg6IDdyZW0gIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogNXJlbTtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbn1cblxuLmVycm9yLXRleHQge1xuICBjb2xvcjogIzlkOWZhNjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgZm9udC1zaXplOiAwLjlyZW07XG4gIG1hcmdpbi1sZWZ0OiA0cmVtO1xuICBtYXJnaW4tcmlnaHQ6IDRyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/core/applications/applications.page.ts":
/*!********************************************************!*\
  !*** ./src/app/core/applications/applications.page.ts ***!
  \********************************************************/
/*! exports provided: ApplicationsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationsPage", function() { return ApplicationsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_shared_services_applications_applications_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/applications/applications.service */ "./src/app/shared/services/applications/applications.service.ts");
/* harmony import */ var src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/services/auth/auth.service */ "./src/app/shared/services/auth/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





let ApplicationsPage = class ApplicationsPage {
    constructor(applicationService, authService, router) {
        this.applicationService = applicationService;
        this.authService = authService;
        this.router = router;
        // Data
        this.applications = [];
        // Checker
        this.isApplicationsEmpty = true;
        // Image
        this.iconError = 'assets/img/icon/error-404.svg';
        this.getData();
    }
    ngOnInit() {
    }
    getData() {
        if (this.authService.userType == 'AP') {
            this.applicationService.getAll().subscribe(() => {
                this.applications = this.applicationService.applications;
            });
        }
    }
};
ApplicationsPage.ctorParameters = () => [
    { type: src_app_shared_services_applications_applications_service__WEBPACK_IMPORTED_MODULE_2__["ApplicationsService"] },
    { type: src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
ApplicationsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-applications',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./applications.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/applications/applications.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./applications.page.scss */ "./src/app/core/applications/applications.page.scss")).default]
    })
], ApplicationsPage);



/***/ })

}]);
//# sourceMappingURL=core-applications-applications-module-es2015.js.map