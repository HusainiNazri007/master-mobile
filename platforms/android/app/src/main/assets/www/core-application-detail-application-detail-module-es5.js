function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-application-detail-application-detail-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/application-detail/application-detail.page.html":
  /*!************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/application-detail/application-detail.page.html ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreApplicationDetailApplicationDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>application-detail</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/core/application-detail/application-detail-routing.module.ts":
  /*!******************************************************************************!*\
    !*** ./src/app/core/application-detail/application-detail-routing.module.ts ***!
    \******************************************************************************/

  /*! exports provided: ApplicationDetailPageRoutingModule */

  /***/
  function srcAppCoreApplicationDetailApplicationDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ApplicationDetailPageRoutingModule", function () {
      return ApplicationDetailPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _application_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./application-detail.page */
    "./src/app/core/application-detail/application-detail.page.ts");

    var routes = [{
      path: '',
      component: _application_detail_page__WEBPACK_IMPORTED_MODULE_3__["ApplicationDetailPage"]
    }];

    var ApplicationDetailPageRoutingModule = function ApplicationDetailPageRoutingModule() {
      _classCallCheck(this, ApplicationDetailPageRoutingModule);
    };

    ApplicationDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ApplicationDetailPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/core/application-detail/application-detail.module.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/core/application-detail/application-detail.module.ts ***!
    \**********************************************************************/

  /*! exports provided: ApplicationDetailPageModule */

  /***/
  function srcAppCoreApplicationDetailApplicationDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ApplicationDetailPageModule", function () {
      return ApplicationDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _application_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./application-detail-routing.module */
    "./src/app/core/application-detail/application-detail-routing.module.ts");
    /* harmony import */


    var _application_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./application-detail.page */
    "./src/app/core/application-detail/application-detail.page.ts");

    var ApplicationDetailPageModule = function ApplicationDetailPageModule() {
      _classCallCheck(this, ApplicationDetailPageModule);
    };

    ApplicationDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _application_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["ApplicationDetailPageRoutingModule"]],
      declarations: [_application_detail_page__WEBPACK_IMPORTED_MODULE_6__["ApplicationDetailPage"]]
    })], ApplicationDetailPageModule);
    /***/
  },

  /***/
  "./src/app/core/application-detail/application-detail.page.scss":
  /*!**********************************************************************!*\
    !*** ./src/app/core/application-detail/application-detail.page.scss ***!
    \**********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreApplicationDetailApplicationDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvYXBwbGljYXRpb24tZGV0YWlsL2FwcGxpY2F0aW9uLWRldGFpbC5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/core/application-detail/application-detail.page.ts":
  /*!********************************************************************!*\
    !*** ./src/app/core/application-detail/application-detail.page.ts ***!
    \********************************************************************/

  /*! exports provided: ApplicationDetailPage */

  /***/
  function srcAppCoreApplicationDetailApplicationDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ApplicationDetailPage", function () {
      return ApplicationDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var ApplicationDetailPage = /*#__PURE__*/function () {
      function ApplicationDetailPage() {
        _classCallCheck(this, ApplicationDetailPage);
      }

      _createClass(ApplicationDetailPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ApplicationDetailPage;
    }();

    ApplicationDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-application-detail',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./application-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/application-detail/application-detail.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./application-detail.page.scss */
      "./src/app/core/application-detail/application-detail.page.scss"))["default"]]
    })], ApplicationDetailPage);
    /***/
  }
}]);
//# sourceMappingURL=core-application-detail-application-detail-module-es5.js.map