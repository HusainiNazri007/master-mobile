function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-faq-faq-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/expandable/expandable.component.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/expandable/expandable.component.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsExpandableExpandableComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div #expandWrapper class=\"expand-wrapper\" [class.collapsed]=\"!expanded\">\n  <ng-content></ng-content>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/faq/faq.page.html":
  /*!******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/faq/faq.page.html ***!
    \******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreFaqFaqPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n\t<ion-toolbar mode=\"ios\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button defaultHref=\"/core/profile\" routerLink=\"/core/profile\" routerDirection=\"back\">\n\t\t\t</ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>FAQ</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen>\n\t<ion-card\n\t\t*ngFor=\"let faq of faqs; let i = index;\" \n\t\tclass=\"accordian-list\" \n\t\tmode=\"ios\"\n\t\tbutton\n\t\t[ngClass]=\"{'section-active': faq.open, 'section': !faq.open}\"\n\t\t(click)=\"toggleSelection(i)\"\n\t>\n\t\t<ion-card-header color=\"light\" class=\"ion-no-padding ion-padding-start\">\n\t\t\t<ion-item lines=\"none\" class=\"ion-no-padding\" color=\"light\">\n\t\t\t\t<ion-icon slot=\"start\" name=\"add-outline\" *ngIf=\"!faq.open\" color=\"primary\"></ion-icon>\n\t\t\t\t<ion-icon slot=\"start\" name=\"remove-outline\" *ngIf=\"faq.open\" color=\"primary\"></ion-icon>\n\t\t\t\t<ion-label class=\"ion-text-wrap\">\n\t\t\t\t\t{{faq.question}}\n\t\t\t\t</ion-label>\n\t\t\t</ion-item>\n\t\t</ion-card-header>\n\n\t\t<ion-card-content *ngIf=\"faq.open\" class=\"ion-padding-top ion-text-wrap ion-justify-content-between\">\n\t\t\t{{faq.answer}}\n\t\t</ion-card-content>\n\t</ion-card>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/components/expandable/expandable.component.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/components/expandable/expandable.component.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsExpandableExpandableComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".expand-wrapper {\n  transition: max-height 0.4s ease-in-out;\n  overflow: hidden;\n  height: auto;\n}\n\n.collapsed {\n  max-height: 0 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9naGctZS1yZWJhdC9zcmMvYXBwL2NvbXBvbmVudHMvZXhwYW5kYWJsZS9leHBhbmRhYmxlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2V4cGFuZGFibGUvZXhwYW5kYWJsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FDQ0o7O0FERUE7RUFDSSx3QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9leHBhbmRhYmxlL2V4cGFuZGFibGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhwYW5kLXdyYXBwZXIge1xuICAgIHRyYW5zaXRpb246IG1heC1oZWlnaHQgMC40cyBlYXNlLWluLW91dDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGhlaWdodDogYXV0bztcbn1cbiAgXG4uY29sbGFwc2VkIHtcbiAgICBtYXgtaGVpZ2h0OiAwICFpbXBvcnRhbnQ7XG59IiwiLmV4cGFuZC13cmFwcGVyIHtcbiAgdHJhbnNpdGlvbjogbWF4LWhlaWdodCAwLjRzIGVhc2UtaW4tb3V0O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBoZWlnaHQ6IGF1dG87XG59XG5cbi5jb2xsYXBzZWQge1xuICBtYXgtaGVpZ2h0OiAwICFpbXBvcnRhbnQ7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/components/expandable/expandable.component.ts":
  /*!***************************************************************!*\
    !*** ./src/app/components/expandable/expandable.component.ts ***!
    \***************************************************************/

  /*! exports provided: ExpandableComponent */

  /***/
  function srcAppComponentsExpandableExpandableComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ExpandableComponent", function () {
      return ExpandableComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var ExpandableComponent = /*#__PURE__*/function () {
      function ExpandableComponent(renderer) {
        _classCallCheck(this, ExpandableComponent);

        this.renderer = renderer;
        this.expanded = false;
        this.expandHeight = "150px";
      }

      _createClass(ExpandableComponent, [{
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          this.renderer.setStyle(this.expandWrapper.nativeElement, "max-height", this.expandHeight);
        }
      }]);

      return ExpandableComponent;
    }();

    ExpandableComponent.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]
      }];
    };

    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("expandWrapper", {
      read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
    })], ExpandableComponent.prototype, "expandWrapper", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("expanded")], ExpandableComponent.prototype, "expanded", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("expandHeight")], ExpandableComponent.prototype, "expandHeight", void 0);
    ExpandableComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-expandable",
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./expandable.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/expandable/expandable.component.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./expandable.component.scss */
      "./src/app/components/expandable/expandable.component.scss"))["default"]]
    })], ExpandableComponent);
    /***/
  },

  /***/
  "./src/app/core/faq/faq-routing.module.ts":
  /*!************************************************!*\
    !*** ./src/app/core/faq/faq-routing.module.ts ***!
    \************************************************/

  /*! exports provided: FaqPageRoutingModule */

  /***/
  function srcAppCoreFaqFaqRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FaqPageRoutingModule", function () {
      return FaqPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _faq_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./faq.page */
    "./src/app/core/faq/faq.page.ts");

    var routes = [{
      path: '',
      component: _faq_page__WEBPACK_IMPORTED_MODULE_3__["FaqPage"]
    }];

    var FaqPageRoutingModule = function FaqPageRoutingModule() {
      _classCallCheck(this, FaqPageRoutingModule);
    };

    FaqPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], FaqPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/core/faq/faq.module.ts":
  /*!****************************************!*\
    !*** ./src/app/core/faq/faq.module.ts ***!
    \****************************************/

  /*! exports provided: FaqPageModule */

  /***/
  function srcAppCoreFaqFaqModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FaqPageModule", function () {
      return FaqPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _faq_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./faq-routing.module */
    "./src/app/core/faq/faq-routing.module.ts");
    /* harmony import */


    var _faq_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./faq.page */
    "./src/app/core/faq/faq.page.ts");
    /* harmony import */


    var src_app_components_expandable_expandable_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/components/expandable/expandable.component */
    "./src/app/components/expandable/expandable.component.ts");

    var FaqPageModule = function FaqPageModule() {
      _classCallCheck(this, FaqPageModule);
    };

    FaqPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _faq_routing_module__WEBPACK_IMPORTED_MODULE_5__["FaqPageRoutingModule"]],
      declarations: [_faq_page__WEBPACK_IMPORTED_MODULE_6__["FaqPage"], src_app_components_expandable_expandable_component__WEBPACK_IMPORTED_MODULE_7__["ExpandableComponent"]]
    })], FaqPageModule);
    /***/
  },

  /***/
  "./src/app/core/faq/faq.page.scss":
  /*!****************************************!*\
    !*** ./src/app/core/faq/faq.page.scss ***!
    \****************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreFaqFaqPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".card-content-ios {\n  padding-top: 10px;\n  padding-bottom: 10px;\n  display: grid;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9naGctZS1yZWJhdC9zcmMvYXBwL2NvcmUvZmFxL2ZhcS5wYWdlLnNjc3MiLCJzcmMvYXBwL2NvcmUvZmFxL2ZhcS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsYUFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29yZS9mYXEvZmFxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLWNvbnRlbnQtaW9zIHtcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICBkaXNwbGF5OiBncmlkO1xufSIsIi5jYXJkLWNvbnRlbnQtaW9zIHtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBkaXNwbGF5OiBncmlkO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/core/faq/faq.page.ts":
  /*!**************************************!*\
    !*** ./src/app/core/faq/faq.page.ts ***!
    \**************************************/

  /*! exports provided: FaqPage */

  /***/
  function srcAppCoreFaqFaqPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FaqPage", function () {
      return FaqPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var FaqPage = /*#__PURE__*/function () {
      function FaqPage(http) {
        _classCallCheck(this, FaqPage);

        this.http = http;
        this.faqs = [];
        this.autoClose = false;
        this.getData();
      }

      _createClass(FaqPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "getData",
        value: function getData() {
          var _this = this;

          this.http.get('assets/data/faq.json').subscribe(function (res) {
            _this.faqs = res['faqs'];
          });
        }
      }, {
        key: "toggleSelection",
        value: function toggleSelection(index) {
          this.faqs[index].open = !this.faqs[index].open;

          if (this.autoClose && this.faqs[index].open) {
            this.faqs.filter(function (faq, faqIndex) {
              faqIndex != index;
            }).map(function (faq) {
              faq.open = false;
            });
          }
        }
      }]);

      return FaqPage;
    }();

    FaqPage.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    FaqPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-faq',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./faq.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/faq/faq.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./faq.page.scss */
      "./src/app/core/faq/faq.page.scss"))["default"]]
    })], FaqPage); // {
    //   "faqs": [
    //       {
    //           "question": "Bilakah permohonan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020?",
    //           "answer": "Permohonan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020 bermula pada 1 April 2020 hingga 24 Julai 2020."
    //       },
    //       {
    //           "question": "Bagaimana cara untuk saya membuat Permohonan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020?",
    //           "answer": "Anda boleh membuat Permohonan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah\r\nMesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020 melalui:\r\n\r\nPermohonan Secara Dalam Talian\r\na. Portal Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau\r\nBerkarbon Rendah Petaling Jaya 2020, https://; atau\r\nb. Aplikasi E-Rebate dengan memuat turun di Playstore / App Store pada telefon\r\nbimbit"
    //       },
    //       {
    //           "question": "Siapakah yang layak memohon Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020?",
    //           "answer": "Pemilik rumah di Petaling Jaya"
    //       },
    //       {
    //           "question": "Apakah kriteria kelayakan bagi pemohon Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020?",
    //           "answer": "Pemohon Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020 perlulah memenuhi kelayakan kriteria seperti berikut:\r\na. Rumah mesti diduduki oleh Pemilik Asal\r\nb. Rumah yang disewakan adalah TIDAK LAYAK untuk memohon\r\nc. Tiada tunggakan cukai taksiran\r\nd. Salinan bil cukai taksiran terkini yang telah dijelaskan"
    //       },
    //       {
    //           "question": "Apakah dokumen yang perlu dikemukakan semasa membuat permohonan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020?",
    //           "answer": "Salinan dokumen sokongan pemohon yang perlu dikemukakan adalah seperti berikut:\r\na. Kad pengenalan pemilik rumah\r\nb. Bil Air 3 bulan Terkini\r\nc. Bil Elektrik 3 bulan terkini"
    //       },
    //       {
    //           "id": "0747219e-9216-4892-ab9f-7efa3f8612b5",
    //           "question": "Saya telah menghantar permohonan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020. Sekiranya ada perubahan maklumat, adakah saya masih boleh membuat kemas kini permohonan?",
    //           "answer": "Kemaskini maklumat permohonan tidak boleh dibuat setelah permohonan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020 dihantar. Pemohon dikehendaki mengesahkan maklumat terlebih dahulu sebelum menghantar permohonan. Permohonan yang tidak lengkap akan ditolak."
    //       },
    //       {
    //           "question": "Bagaimana cara untuk saya mengetahui status permohonan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020?",
    //           "answer": "Semakan Status permohonan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020 boleh dibuat seperti berikut:\r\ni. Portal Rasmi Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020, https://\r\nii. Semakan secara Aplikasi Mobile E-Rebate di Playstore bagi platform Android dan di Apps Store bagi platform iOS"
    //       },
    //       {
    //           "question": "Bagaimanan cara saya mendapatkan maklumat terkini, membuat pertanyaan atau aduan berkaitan Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020?",
    //           "answer": "Anda boleh mendapatkan maklumat terkini, membuat pertanyaan atau aduan melalui saluran berikut:\r\ni. Portal Rasmi Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020, https://\r\nii. Aplikasi Mobile E-Rebate"
    //       },
    //       {
    //           "question": "Jika seseorang memberi maklumat palsu, apakah tindakan yang akan dikenakan?",
    //           "answer": "Permohonan akan ditolak dan pemohon boleh dikenakan tindakan undang-undang."
    //       },
    //       {
    //           "question": "Bilakah panel penilai Skim Rebat Cukai Taksiran Bagi Pemilik Rumah Mesra Alam Hijau Berkarbon Rendah Petaling Jaya 2020 akan mengadakan lawatan ke rumah pemohon?",
    //           "answer": "Panel penilai hanya akan mengadakan lawatan ke rumah pemohon sekiranya bukti yang dikemukakan adalah meragukan dan memerlukan on-site verification. Pemohon akan dimaklumkan tarikh lawatan ke rumah melalui portal skim rebat atau aplikasi E-Rebate dari semasa ke semasa."
    //       },
    //       {
    //           "question": "Adakah rayuan boleh dibuat setelah keputusan diperolehi?",
    //           "answer": "Sebarang rayuan untuk meminda keputusan kelayakan rebat adalah TIDAK DIBENARKAN."
    //       },
    //       {
    //           "question": "Bilakah rebat cukai taksiran akan dikreditkan ke dalam akaun cukai taksiran pemohon?",
    //           "answer": "Rebat cukai taksiran akan dikreditkan ke dalam akaun cukai taksiran selewat-lewatnya pada penggal ke-2 (Julai – Disember) tahun 2021."
    //       },
    //       {
    //           "question": "Adakah bil cukai taksiran penggal pertama (Jan – Jun) 2021 perlu dijelaskan jika rebat cukai masih tidak dikreditkan?",
    //           "answer": "Bil penggal pertama (Jan – Jun) 2021 masih perlu dijelaskan walaupun tiada rebat cukai dikreditkan."
    //       },
    //       {
    //           "question": "Adakah permohonan yang gagal akan dimaklumkan?",
    //           "answer": "Ya, pemohon akan menerima pemakluman melalui portal Skim Rebat atau Aplikasi E-Rebate."
    //       },
    //       {
    //           "question": "Adakah boleh memohon sub-aspek yang sama setiap tahun?",
    //           "answer": "Boleh memohon sub-aspek yang sama setiap tahun dengan syarat terdapat 50% penambahbaikan yang dibuat. Walaubagaimanapun pengesahan akhir akan dibuat oleh panel penilai MBPJ."
    //       }
    //   ]
    // }

    /***/
  }
}]);
//# sourceMappingURL=core-faq-faq-module-es5.js.map