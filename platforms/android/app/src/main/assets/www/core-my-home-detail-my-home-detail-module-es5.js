function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-my-home-detail-my-home-detail-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/my-home-detail/my-home-detail.page.html":
  /*!****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/my-home-detail/my-home-detail.page.html ***!
    \****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreMyHomeDetailMyHomeDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n\t<ion-toolbar mode=\"ios\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button \n\t\t\t\tdefaultHref=\"/core/my-home\"\n\t\t\t\trouterLink=\"/core/my-home\"\n\t\t\t\trouterDirection=\"back\"\n\t\t\t\ttext=\"\"\n\t\t\t>\n\t\t\t</ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>Home detail</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen>\n  <ion-list class=\"ion-no-margin list-core\" mode=\"ios\">\n\t\t<form [formGroup]=\"houseForm\">\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Address</ion-label>\n        <ion-textarea \n          placeholder=\"No. 23, Jalan PJU 9/1\"\n          mode=\"ios\"\n          formControlName=\"address\"\n          [value]=\"house.address\"\n        >\n        </ion-textarea>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Postcode</ion-label>\n        <ion-input\n          placeholder=\"40150\"\n          mode=\"ios\"\n          formControlName=\"postcode\"\n          type=\"number\"\n          value]=\"house.postcode\"\n        >\n        </ion-input>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Area</ion-label>\n\t\t\t\t<ion-select placeholder=\"Damansara Perdana\" mode=\"ios\" formControlName=\"area\" [value]=\"house.area\">\n\t\t\t\t\t<ion-select-option *ngFor=\"let area of areaOptions\" value=\"occupant.value\">{{area.value}}</ion-select-option>\n\t\t\t\t</ion-select>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Assessment Tax Account No.</ion-label>\n        <ion-input\n          placeholder=\"T10918210010010\"\n          mode=\"ios\"\n          formControlName=\"assessment_tax_account\"\n          type=\"number\"\n          [value]=\"house.assessment_tax_account\"\n        >\n\t\t\t\t</ion-input>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Staying Since</ion-label>\n        <ion-datetime\n          placeholder=\"March 2010\"\n          mode=\"ios\"\n          formControlName=\"staying_duration_since\"\n          displayFormat=\"MMMM YYYY\"\n          [value]=\"house.staying_duration_since\"\n        >\n\t\t\t\t</ion-datetime>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Occupants No.</ion-label>\n\t\t\t\t<ion-select placeholder=\"1\" mode=\"ios\" formControlName=\"occupants\" [value]=\"house.occupants\">\n\t\t\t\t\t<ion-select-option *ngFor=\"let occupant of occupantOptions\" value=\"occupant.value\">{{occupant.value}}</ion-select-option>\n\t\t\t\t</ion-select>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item  class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Assessment Tax Document</ion-label>\n\t\t\t\t<ion-button mode=\"ios\" color=\"primary\" expand=\"block\" (click)=\"openUploadSheet()\">Upload</ion-button>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\" *ngIf=\"!imageSelected && !house.assessment_tax_doc\">\n\t\t\t\t<ion-img [src]=\"imageSelected\"></ion-img>\n\t\t\t</ion-item>\n\t\t</form>\n\t</ion-list>\n</ion-content>\n\n<ion-footer class=\"ion-no-border ion-padding\" mode=\"ios\">\n\t<ion-button \n\t\t*ngIf=\"!isLoading\" \n\t\texpand=\"block\" \n\t\tmode=\"ios\" \n\t\tshape=\"round\" \n\t\tcolor=\"primary\" \n\t\t[disabled]=\"!houseForm.valid\"\n\t\ttype=\"submit\"\n\t\t(click)=\"update()\"\n\t>\n\t\tUpdate\n\t</ion-button>\n\t<ion-button \n\t\t*ngIf=\"isLoading\" \n\t\texpand=\"block\" \n\t\tmode=\"ios\"\n\t\tshape=\"round\"\n\t\tcolor=\"primary\"\n\t>\n\t\t<ion-spinner name=\"bubbles\"></ion-spinner>\n\t</ion-button>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/core/my-home-detail/my-home-detail-routing.module.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/core/my-home-detail/my-home-detail-routing.module.ts ***!
    \**********************************************************************/

  /*! exports provided: MyHomeDetailPageRoutingModule */

  /***/
  function srcAppCoreMyHomeDetailMyHomeDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyHomeDetailPageRoutingModule", function () {
      return MyHomeDetailPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _my_home_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./my-home-detail.page */
    "./src/app/core/my-home-detail/my-home-detail.page.ts");

    var routes = [{
      path: '',
      component: _my_home_detail_page__WEBPACK_IMPORTED_MODULE_3__["MyHomeDetailPage"]
    }];

    var MyHomeDetailPageRoutingModule = function MyHomeDetailPageRoutingModule() {
      _classCallCheck(this, MyHomeDetailPageRoutingModule);
    };

    MyHomeDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], MyHomeDetailPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/core/my-home-detail/my-home-detail.module.ts":
  /*!**************************************************************!*\
    !*** ./src/app/core/my-home-detail/my-home-detail.module.ts ***!
    \**************************************************************/

  /*! exports provided: MyHomeDetailPageModule */

  /***/
  function srcAppCoreMyHomeDetailMyHomeDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyHomeDetailPageModule", function () {
      return MyHomeDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _my_home_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./my-home-detail-routing.module */
    "./src/app/core/my-home-detail/my-home-detail-routing.module.ts");
    /* harmony import */


    var _my_home_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./my-home-detail.page */
    "./src/app/core/my-home-detail/my-home-detail.page.ts");

    var MyHomeDetailPageModule = function MyHomeDetailPageModule() {
      _classCallCheck(this, MyHomeDetailPageModule);
    };

    MyHomeDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _my_home_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyHomeDetailPageRoutingModule"]],
      declarations: [_my_home_detail_page__WEBPACK_IMPORTED_MODULE_6__["MyHomeDetailPage"]]
    })], MyHomeDetailPageModule);
    /***/
  },

  /***/
  "./src/app/core/my-home-detail/my-home-detail.page.scss":
  /*!**************************************************************!*\
    !*** ./src/app/core/my-home-detail/my-home-detail.page.scss ***!
    \**************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreMyHomeDetailMyHomeDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvbXktaG9tZS1kZXRhaWwvbXktaG9tZS1kZXRhaWwucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/core/my-home-detail/my-home-detail.page.ts":
  /*!************************************************************!*\
    !*** ./src/app/core/my-home-detail/my-home-detail.page.ts ***!
    \************************************************************/

  /*! exports provided: MyHomeDetailPage */

  /***/
  function srcAppCoreMyHomeDetailMyHomeDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyHomeDetailPage", function () {
      return MyHomeDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/shared/services/auth/auth.service */
    "./src/app/shared/services/auth/auth.service.ts");
    /* harmony import */


    var src_app_shared_services_houses_houses_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/shared/services/houses/houses.service */
    "./src/app/shared/services/houses/houses.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_assets_data_areas__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/assets/data/areas */
    "./src/assets/data/areas.ts");
    /* harmony import */


    var src_assets_data_occupants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/assets/data/occupants */
    "./src/assets/data/occupants.ts");
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var src_app_shared_handlers_notify_notify_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/shared/handlers/notify/notify.service */
    "./src/app/shared/handlers/notify/notify.service.ts");

    var MyHomeDetailPage = /*#__PURE__*/function () {
      function MyHomeDetailPage(authService, houseService, actionSheetCtrl, camera, fb, toastr) {
        _classCallCheck(this, MyHomeDetailPage);

        this.authService = authService;
        this.houseService = houseService;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.fb = fb;
        this.toastr = toastr; // Type

        this.houseTypes = [{
          value: 'CD',
          text: 'Condominium'
        }, {
          value: 'FL',
          text: 'Flat'
        }, {
          value: 'TO',
          text: 'Townhouse'
        }, {
          value: 'TE',
          text: 'Terrace House'
        }, {
          value: 'BS',
          text: 'Bungalow / Semidetached'
        }, {
          value: 'AS',
          text: 'Apartment / Service Apartment'
        }];
        this.areaOptions = src_assets_data_areas__WEBPACK_IMPORTED_MODULE_6__["Areas"];
        this.occupantOptions = src_assets_data_occupants__WEBPACK_IMPORTED_MODULE_7__["Occupants"]; // Checker

        this.isLoading = false;
      }

      _createClass(MyHomeDetailPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.houseForm = this.fb.group({
            owner: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
            postcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
            area: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
            building_type: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
            assessment_tax_account: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
            assessment_tax_doc: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
            staying_duration_since: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
            occupants: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]))
          });
          this.houseForm.controls['address'].setValue(this.authService.userID);
          this.houseForm.controls['postcode'].setValue(this.authService.userID);
          this.houseForm.controls['area'].setValue(this.authService.userID);
          this.houseForm.controls['building_type'].setValue(this.authService.userID);
          this.houseForm.controls['assessment_tax_account'].setValue(this.authService.userID);
          this.houseForm.controls['assessment_tax_doc'].setValue(this.authService.userID);
          this.houseForm.controls['staying_duration_since'].setValue(this.authService.userID);
          this.houseForm.controls['occupants'].setValue(this.authService.userID);
        }
      }, {
        key: "update",
        value: function update() {
          var _this = this;

          this.isLoading = true;
          this.houseService.update(this.house.id, this.houseForm.value).subscribe(function () {
            // Success
            _this.isLoading = false;
          }, function () {
            // Failed
            _this.isLoading = false;
          }, function () {
            // After
            _this.houseForm.reset();

            _this.toastr.openToastr('');
          });
        }
      }, {
        key: "openUploadSheet",
        value: function openUploadSheet() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this2 = this;

            var actionSheet;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.actionSheetCtrl.create({
                      header: '',
                      // cssClass: 'my-custom-class',
                      buttons: [{
                        text: 'Gallery',
                        icon: 'Selecteds-outline',
                        handler: function handler() {
                          _this2.openGallery();
                        }
                      }, {
                        text: 'Camera',
                        icon: 'camera-outline',
                        handler: function handler() {
                          _this2.openCamera();
                        }
                      }, {
                        text: 'Cancel',
                        icon: 'close-outline',
                        role: 'cancel',
                        handler: function handler() {
                          console.log('Cancel clicked');
                        }
                      }]
                    });

                  case 2:
                    actionSheet = _context.sent;
                    _context.next = 5;
                    return actionSheet.present();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "openGallery",
        value: function openGallery() {
          var _this3 = this;

          // console.log('Gallery opened')
          var cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.FILE_URI,
            quality: 60,
            targetWidth: 1000,
            targetHeight: 1000,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
          };
          this.camera.getPicture(cameraOptions).then(function (file_uri) {
            _this3.imageSelected = file_uri;
          }, function (err) {
            console.log(err);
          });
        }
      }, {
        key: "openCamera",
        value: function openCamera() {
          var _this4 = this;

          // console.log('Camera opened')
          var options = {
            quality: 60,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
          };
          this.camera.getPicture(options).then(function (imageData) {
            _this4.imageSelected = imageData; //this.tempImage[billNumber] = (<any>window).Ionic.WebView.convertFileSrc(imageData);
          }, function (err) {
            alert("error " + JSON.stringify(err));
          });
        }
      }]);

      return MyHomeDetailPage;
    }();

    MyHomeDetailPage.ctorParameters = function () {
      return [{
        type: src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
      }, {
        type: src_app_shared_services_houses_houses_service__WEBPACK_IMPORTED_MODULE_4__["HousesService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ActionSheetController"]
      }, {
        type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_8__["Camera"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: src_app_shared_handlers_notify_notify_service__WEBPACK_IMPORTED_MODULE_9__["NotifyService"]
      }];
    };

    MyHomeDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-my-home-detail',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./my-home-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/my-home-detail/my-home-detail.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./my-home-detail.page.scss */
      "./src/app/core/my-home-detail/my-home-detail.page.scss"))["default"]]
    })], MyHomeDetailPage);
    /***/
  }
}]);
//# sourceMappingURL=core-my-home-detail-my-home-detail-module-es5.js.map