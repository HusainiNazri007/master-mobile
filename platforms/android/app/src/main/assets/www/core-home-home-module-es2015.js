(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/home/home.page.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/home/home.page.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Home</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  <ion-grid mode=\"ios\" class=\"ion-no-margin\">\n    <ion-row mode=\"ios\" class=\"ion-no-margin\">\n      <ion-col mode=\"ios\" class=\"ion-no-padding\">\n        <ion-card mode=\"ios\" class=\"ion-margin\" (click)=\"navigatePage('/core/my-home')\">\n          <img [src]=\"imgMyHome\">\n          <ion-card-content class=\"ion-no-padding\">\n            <h5 class=\"ion-text-center ion-no-margin ion-padding\">MyHome</h5>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n\n      <ion-col mode=\"ios\" class=\"ion-no-padding\">\n        <ion-card mode=\"ios\" class=\"ion-margin\" (click)=\"navigatePage('/core/application/walkthrough')\">\n          <img [src]=\"imgApply\">\n          <ion-card-content class=\"ion-no-padding\">\n            <h5 class=\"ion-text-center ion-no-margin ion-padding\">Apply eRebate</h5>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row mode=\"ios\" class=\"ion-no-margin\">\n      <ion-col mode=\"ios\" class=\"ion-no-padding\">\n        <ion-card mode=\"ios\" class=\"ion-margin\" (click)=\"navigatePage('/core/applications')\">\n          <img [src]=\"imgHistory\">\n          <ion-card-content class=\"ion-no-padding\">\n            <h5 class=\"ion-text-center ion-no-margin ion-padding\">History</h5>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n\n      <ion-col mode=\"ios\" class=\"ion-no-padding\">\n        <ion-card mode=\"ios\" class=\"ion-margin\" (click)=\"navigatePage('/core/notifications')\">\n          <img [src]=\"imgNotification\">\n          <ion-card-content class=\"ion-no-padding\">\n            <h5 class=\"ion-text-center ion-no-margin ion-padding\">Notifications</h5>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-list mode=\"ios\" class=\"ion-no-margin list-core\">\n    <ion-list-header mode=\"ios\">\n      <h3>Status</h3>\n    </ion-list-header>\n\n    <ion-card mode=\"ios\" [hidden]=\"isApplicationEmpty\">\n      <ion-card-content>\n        <ion-item class=\"ion-no-padding\" lines=\"none\">\n          <ion-label>\n            <h2>{{id_serial}}</h2>\n            <p>Submitted on {{created_at}}</p>\n          </ion-label>\n          <ion-button size=\"small\" shape=\"round\" slot=\"end\">View</ion-button>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n\n    <div [hidden]=\"!isApplicationEmpty\">\n      <img [src]=\"iconError\" class=\"error-img\">\n      <p class=\"error-text\">No application submitted yet</p>\n    </div>\n  </ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/core/home/home-routing.module.ts":
/*!**************************************************!*\
  !*** ./src/app/core/home/home-routing.module.ts ***!
  \**************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/core/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/core/home/home.module.ts":
/*!******************************************!*\
  !*** ./src/app/core/home/home.module.ts ***!
  \******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/core/home/home-routing.module.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/core/home/home.page.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/core/home/home.page.scss":
/*!******************************************!*\
  !*** ./src/app/core/home/home.page.scss ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error-img {\n  display: grid;\n  max-width: 7rem !important;\n  margin-top: 5rem;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.error-text {\n  color: #9d9fa6;\n  display: grid;\n  font-size: 0.9rem;\n  margin-left: auto;\n  margin-right: auto;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9naGctZS1yZWJhdC9zcmMvYXBwL2NvcmUvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvY29yZS9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FERUE7RUFDRSxjQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9jb3JlL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXJyb3ItaW1nIHtcbiAgZGlzcGxheTogZ3JpZDtcbiAgbWF4LXdpZHRoOiA3cmVtICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi10b3A6IDVyZW07XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG5cbi5lcnJvci10ZXh0IHtcbiAgY29sb3I6ICM5ZDlmYTY7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGZvbnQtc2l6ZTogMC45cmVtO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59IiwiLmVycm9yLWltZyB7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIG1heC13aWR0aDogN3JlbSAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiA1cmVtO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xufVxuXG4uZXJyb3ItdGV4dCB7XG4gIGNvbG9yOiAjOWQ5ZmE2O1xuICBkaXNwbGF5OiBncmlkO1xuICBmb250LXNpemU6IDAuOXJlbTtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/core/home/home.page.ts":
/*!****************************************!*\
  !*** ./src/app/core/home/home.page.ts ***!
  \****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_shared_services_applications_applications_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/services/applications/applications.service */ "./src/app/shared/services/applications/applications.service.ts");




let HomePage = class HomePage {
    constructor(activatedRoute, applicationService, router) {
        this.activatedRoute = activatedRoute;
        this.applicationService = applicationService;
        this.router = router;
        this.role = '';
        // Checker
        this.isApplicationEmpty = true;
        // Data
        this.applications = [];
        this.id_serial = 21412;
        this.created_at = '22/04/2020';
        // Image
        this.imgMyHome = 'assets/img/default/house.jpg';
        this.imgApply = 'assets/img/default/form.jpg';
        this.imgNotification = 'assets/img/default/notification.jpg';
        this.imgHistory = 'assets/img/default/bookshelf.jpg';
        this.iconError = 'assets/img/icon/error-404.svg';
        this.getData();
    }
    ngOnInit() {
        this.folder = this.activatedRoute.snapshot.paramMap.get('id');
    }
    getData() {
        this.applicationService.getAll().subscribe(() => {
            this.applications = this.applicationService.applications;
        }, () => { }, () => { });
    }
    navigatePage(path) {
        this.router.navigate([path]);
    }
    navigateApplication(application) {
        this.router.navigate(['/core/application-detail/', application]);
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_shared_services_applications_applications_service__WEBPACK_IMPORTED_MODULE_3__["ApplicationsService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/core/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=core-home-home-module-es2015.js.map