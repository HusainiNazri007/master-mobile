function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-my-home-my-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/my-home/my-home.page.html":
  /*!**************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/my-home/my-home.page.html ***!
    \**************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreMyHomeMyHomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>MyHome</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content [fullscreen]=\"true\">\n  <ion-list mode=\"ios\" class=\"ion-no-margin list-core\">\n    <ion-list-header mode=\"ios\">\n      <h3>Registered Homes</h3>\n    </ion-list-header>\n\n    <ion-card mode=\"ios\" [hidden]=\"isMyHomesEmpty\" *ngFor=\"let house of houses\">\n      <ion-card-content class=\"ion-no-padding ion-padding-start ion-padding-end\">\n        <ion-item class=\"ion-no-padding\" lines=\"none\">\n          <ion-icon name=\"home-outline\" slot=\"start\" color=\"primary\"></ion-icon>\n          <ion-label class=\"ion-text-wrap\">\n            <h3>{{house.assessment_tax_account}}</h3>\n            <h4 *ngIf=\"house.building_type == 'CD'\">Condominium</h4>\n            <h4 *ngIf=\"house.building_type == 'FL'\">Flat</h4>\n            <h4 *ngIf=\"house.building_type == 'TO'\">Townhouse</h4>\n            <h4 *ngIf=\"house.building_type == 'TE'\">Terrace House</h4>\n            <h4 *ngIf=\"house.building_type == 'BS'\">Bungalow / Semidetached</h4>\n            <h4 *ngIf=\"house.building_type == 'AS'\">Apartment / Service Apartment</h4>\n            <h4 *ngIf=\"house.building_type == 'OT'\">Other</h4>\n            <p>{{house.address}}, {{house.postcode}} {{house.area}}, Petaling Jaya, Selangor</p>\n          </ion-label>\n          <ion-button size=\"small\" shape=\"round\" slot=\"end\">View</ion-button>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n\n    <div [hidden]=\"!isMyHomesEmpty\">\n      <img [src]=\"iconError\" class=\"error-img\">\n      <p class=\"error-text\">No home registered yet, please register using the button below</p>\n    </div>\n  </ion-list>\n\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\" (click)=\"addHome('/my-home/add/')\">\n    <ion-fab-button>\n      <ion-icon name=\"add-circle-outline\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/core/my-home/my-home-routing.module.ts":
  /*!********************************************************!*\
    !*** ./src/app/core/my-home/my-home-routing.module.ts ***!
    \********************************************************/

  /*! exports provided: MyHomePageRoutingModule */

  /***/
  function srcAppCoreMyHomeMyHomeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyHomePageRoutingModule", function () {
      return MyHomePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _my_home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./my-home.page */
    "./src/app/core/my-home/my-home.page.ts");

    var routes = [{
      path: '',
      component: _my_home_page__WEBPACK_IMPORTED_MODULE_3__["MyHomePage"]
    }];

    var MyHomePageRoutingModule = function MyHomePageRoutingModule() {
      _classCallCheck(this, MyHomePageRoutingModule);
    };

    MyHomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], MyHomePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/core/my-home/my-home.module.ts":
  /*!************************************************!*\
    !*** ./src/app/core/my-home/my-home.module.ts ***!
    \************************************************/

  /*! exports provided: MyHomePageModule */

  /***/
  function srcAppCoreMyHomeMyHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyHomePageModule", function () {
      return MyHomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _my_home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./my-home-routing.module */
    "./src/app/core/my-home/my-home-routing.module.ts");
    /* harmony import */


    var _my_home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./my-home.page */
    "./src/app/core/my-home/my-home.page.ts");

    var MyHomePageModule = function MyHomePageModule() {
      _classCallCheck(this, MyHomePageModule);
    };

    MyHomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _my_home_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyHomePageRoutingModule"]],
      declarations: [_my_home_page__WEBPACK_IMPORTED_MODULE_6__["MyHomePage"]]
    })], MyHomePageModule);
    /***/
  },

  /***/
  "./src/app/core/my-home/my-home.page.scss":
  /*!************************************************!*\
    !*** ./src/app/core/my-home/my-home.page.scss ***!
    \************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreMyHomeMyHomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".error-img {\n  display: grid;\n  max-width: 7rem !important;\n  margin-top: 5rem;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.error-text {\n  color: #9d9fa6;\n  display: grid;\n  font-size: 0.9rem;\n  margin-left: 4rem;\n  margin-right: 4rem;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9naGctZS1yZWJhdC9zcmMvYXBwL2NvcmUvbXktaG9tZS9teS1ob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvY29yZS9teS1ob21lL215LWhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxjQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb3JlL215LWhvbWUvbXktaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXJyb3ItaW1nIHtcbiAgICBkaXNwbGF5OiBncmlkO1xuICAgIG1heC13aWR0aDogN3JlbSAhaW1wb3J0YW50O1xuICAgIG1hcmdpbi10b3A6IDVyZW07XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xufVxuICBcbi5lcnJvci10ZXh0IHtcbiAgICBjb2xvcjogIzlkOWZhNjtcbiAgICBkaXNwbGF5OiBncmlkO1xuICAgIGZvbnQtc2l6ZTogMC45cmVtO1xuICAgIG1hcmdpbi1sZWZ0OiA0cmVtO1xuICAgIG1hcmdpbi1yaWdodDogNHJlbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59IiwiLmVycm9yLWltZyB7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIG1heC13aWR0aDogN3JlbSAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiA1cmVtO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xufVxuXG4uZXJyb3ItdGV4dCB7XG4gIGNvbG9yOiAjOWQ5ZmE2O1xuICBkaXNwbGF5OiBncmlkO1xuICBmb250LXNpemU6IDAuOXJlbTtcbiAgbWFyZ2luLWxlZnQ6IDRyZW07XG4gIG1hcmdpbi1yaWdodDogNHJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/core/my-home/my-home.page.ts":
  /*!**********************************************!*\
    !*** ./src/app/core/my-home/my-home.page.ts ***!
    \**********************************************/

  /*! exports provided: MyHomePage */

  /***/
  function srcAppCoreMyHomeMyHomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyHomePage", function () {
      return MyHomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_app_shared_services_houses_houses_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/shared/services/houses/houses.service */
    "./src/app/shared/services/houses/houses.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/shared/services/auth/auth.service */
    "./src/app/shared/services/auth/auth.service.ts");

    var MyHomePage = /*#__PURE__*/function () {
      function MyHomePage(authService, houseService, router) {
        _classCallCheck(this, MyHomePage);

        this.authService = authService;
        this.houseService = houseService;
        this.router = router; // Data

        this.houses = [];
        this.id_serial = 124124;
        this.created_at = '12/05/2020'; // Image

        this.iconError = 'assets/img/icon/error-404.svg'; // Checker

        this.isMyHomesEmpty = true;
        this.getData();
      }

      _createClass(MyHomePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {}
      }, {
        key: "getData",
        value: function getData() {
          var _this = this;

          var fieldOwner = 'owner=' + this.authService.userID;

          if (this.authService.userID) {
            this.houseService.filter(fieldOwner).subscribe(function () {
              _this.houses = _this.houseService.housesFiltered;
            }, function () {}, function () {
              if (_this.houses.length >= 1) {
                _this.isMyHomesEmpty = false;
              }
            });
          }
        }
      }, {
        key: "navigateDetail",
        value: function navigateDetail(house) {}
      }, {
        key: "addHome",
        value: function addHome(path) {
          this.router.navigate([path]);
        }
      }]);

      return MyHomePage;
    }();

    MyHomePage.ctorParameters = function () {
      return [{
        type: src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
      }, {
        type: src_app_shared_services_houses_houses_service__WEBPACK_IMPORTED_MODULE_2__["HousesService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }];
    };

    MyHomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-my-home',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./my-home.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/my-home/my-home.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./my-home.page.scss */
      "./src/app/core/my-home/my-home.page.scss"))["default"]]
    })], MyHomePage);
    /***/
  }
}]);
//# sourceMappingURL=core-my-home-my-home-module-es5.js.map