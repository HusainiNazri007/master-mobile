(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-helpdesk-helpdesk-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/helpdesk/helpdesk.page.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/helpdesk/helpdesk.page.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n\t<ion-toolbar mode=\"ios\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button \n\t\t\t\tdefaultHref=\"/core/profile\"\n\t\t\t\trouterLink=\"/core/profile\"\n\t\t\t\trouterDirection=\"back\"\n\t\t\t>\n\t\t\t</ion-back-button>\n    </ion-buttons>\n    <ion-title>Helpdesk</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\" fullscreen>\n\t<img [src]=\"imgConstruction\">\n\t<p class=\"ion-text-center construction-text\">Page is under construction</p>\n</ion-content>");

/***/ }),

/***/ "./src/app/core/helpdesk/helpdesk-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/core/helpdesk/helpdesk-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: HelpdeskPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpdeskPageRoutingModule", function() { return HelpdeskPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _helpdesk_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helpdesk.page */ "./src/app/core/helpdesk/helpdesk.page.ts");




const routes = [
    {
        path: '',
        component: _helpdesk_page__WEBPACK_IMPORTED_MODULE_3__["HelpdeskPage"]
    }
];
let HelpdeskPageRoutingModule = class HelpdeskPageRoutingModule {
};
HelpdeskPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HelpdeskPageRoutingModule);



/***/ }),

/***/ "./src/app/core/helpdesk/helpdesk.module.ts":
/*!**************************************************!*\
  !*** ./src/app/core/helpdesk/helpdesk.module.ts ***!
  \**************************************************/
/*! exports provided: HelpdeskPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpdeskPageModule", function() { return HelpdeskPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _helpdesk_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./helpdesk-routing.module */ "./src/app/core/helpdesk/helpdesk-routing.module.ts");
/* harmony import */ var _helpdesk_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./helpdesk.page */ "./src/app/core/helpdesk/helpdesk.page.ts");







let HelpdeskPageModule = class HelpdeskPageModule {
};
HelpdeskPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _helpdesk_routing_module__WEBPACK_IMPORTED_MODULE_5__["HelpdeskPageRoutingModule"]
        ],
        declarations: [_helpdesk_page__WEBPACK_IMPORTED_MODULE_6__["HelpdeskPage"]]
    })
], HelpdeskPageModule);



/***/ }),

/***/ "./src/app/core/helpdesk/helpdesk.page.scss":
/*!**************************************************!*\
  !*** ./src/app/core/helpdesk/helpdesk.page.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".construction-text {\n  color: #9d9fa6;\n  font-size: 0.9rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9naGctZS1yZWJhdC9zcmMvYXBwL2NvcmUvaGVscGRlc2svaGVscGRlc2sucGFnZS5zY3NzIiwic3JjL2FwcC9jb3JlL2hlbHBkZXNrL2hlbHBkZXNrLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29yZS9oZWxwZGVzay9oZWxwZGVzay5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29uc3RydWN0aW9uLXRleHQge1xuICAgIGNvbG9yOiAjOWQ5ZmE2O1xuICAgIGZvbnQtc2l6ZTogMC45cmVtO1xufSIsIi5jb25zdHJ1Y3Rpb24tdGV4dCB7XG4gIGNvbG9yOiAjOWQ5ZmE2O1xuICBmb250LXNpemU6IDAuOXJlbTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/core/helpdesk/helpdesk.page.ts":
/*!************************************************!*\
  !*** ./src/app/core/helpdesk/helpdesk.page.ts ***!
  \************************************************/
/*! exports provided: HelpdeskPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpdeskPage", function() { return HelpdeskPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let HelpdeskPage = class HelpdeskPage {
    constructor() {
        // Image
        this.imgConstruction = 'assets/img/default/Construction.png';
    }
    ngOnInit() {
    }
};
HelpdeskPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-helpdesk',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./helpdesk.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/helpdesk/helpdesk.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./helpdesk.page.scss */ "./src/app/core/helpdesk/helpdesk.page.scss")).default]
    })
], HelpdeskPage);



/***/ })

}]);
//# sourceMappingURL=core-helpdesk-helpdesk-module-es2015.js.map