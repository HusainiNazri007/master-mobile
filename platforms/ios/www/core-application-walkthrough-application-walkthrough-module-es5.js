function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-application-walkthrough-application-walkthrough-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/application-walkthrough/application-walkthrough.page.html":
  /*!**********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/application-walkthrough/application-walkthrough.page.html ***!
    \**********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreApplicationWalkthroughApplicationWalkthroughPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Apply eRebate</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content fullscreen>\n  <ion-slides pager=\"true\" [options]=\"slidesOptions\">\n    <ion-slide *ngFor=\"let page of pages\">\n      <div class=\"content-container\">\n        <img [src]=\"page.path\">\n        <h2 class=\"ion-no-margin ion-margin-top\">{{page.title}}</h2>\n        <p class=\"ion-no-margin\">{{page.text}}</p>\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n\n<ion-footer class=\"ion-no-border ion-padding\" mode=\"ios\">\n  <ion-button expand=\"block\" mode=\"ios\" shape=\"round\" (click)=\"navigatePage('/application/apply')\">Start</ion-button>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/core/application-walkthrough/application-walkthrough-routing.module.ts":
  /*!****************************************************************************************!*\
    !*** ./src/app/core/application-walkthrough/application-walkthrough-routing.module.ts ***!
    \****************************************************************************************/

  /*! exports provided: ApplicationWalkthroughPageRoutingModule */

  /***/
  function srcAppCoreApplicationWalkthroughApplicationWalkthroughRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ApplicationWalkthroughPageRoutingModule", function () {
      return ApplicationWalkthroughPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _application_walkthrough_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./application-walkthrough.page */
    "./src/app/core/application-walkthrough/application-walkthrough.page.ts");

    var routes = [{
      path: '',
      component: _application_walkthrough_page__WEBPACK_IMPORTED_MODULE_3__["ApplicationWalkthroughPage"]
    }];

    var ApplicationWalkthroughPageRoutingModule = function ApplicationWalkthroughPageRoutingModule() {
      _classCallCheck(this, ApplicationWalkthroughPageRoutingModule);
    };

    ApplicationWalkthroughPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ApplicationWalkthroughPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/core/application-walkthrough/application-walkthrough.module.ts":
  /*!********************************************************************************!*\
    !*** ./src/app/core/application-walkthrough/application-walkthrough.module.ts ***!
    \********************************************************************************/

  /*! exports provided: ApplicationWalkthroughPageModule */

  /***/
  function srcAppCoreApplicationWalkthroughApplicationWalkthroughModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ApplicationWalkthroughPageModule", function () {
      return ApplicationWalkthroughPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _application_walkthrough_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./application-walkthrough-routing.module */
    "./src/app/core/application-walkthrough/application-walkthrough-routing.module.ts");
    /* harmony import */


    var _application_walkthrough_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./application-walkthrough.page */
    "./src/app/core/application-walkthrough/application-walkthrough.page.ts");

    var ApplicationWalkthroughPageModule = function ApplicationWalkthroughPageModule() {
      _classCallCheck(this, ApplicationWalkthroughPageModule);
    };

    ApplicationWalkthroughPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _application_walkthrough_routing_module__WEBPACK_IMPORTED_MODULE_5__["ApplicationWalkthroughPageRoutingModule"]],
      declarations: [_application_walkthrough_page__WEBPACK_IMPORTED_MODULE_6__["ApplicationWalkthroughPage"]]
    })], ApplicationWalkthroughPageModule);
    /***/
  },

  /***/
  "./src/app/core/application-walkthrough/application-walkthrough.page.scss":
  /*!********************************************************************************!*\
    !*** ./src/app/core/application-walkthrough/application-walkthrough.page.scss ***!
    \********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreApplicationWalkthroughApplicationWalkthroughPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".content-container {\n  display: grid;\n  align-items: center;\n  align-content: center;\n  justify-content: center;\n  justify-items: center;\n  width: 100vh;\n  height: 80vh;\n}\n.content-container img {\n  width: 16rem;\n}\n.content-container p {\n  margin-top: 1rem;\n  padding-left: 3rem;\n  padding-right: 3rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9tb2JpbGUvc3JjL2FwcC9jb3JlL2FwcGxpY2F0aW9uLXdhbGt0aHJvdWdoL2FwcGxpY2F0aW9uLXdhbGt0aHJvdWdoLnBhZ2Uuc2NzcyIsInNyYy9hcHAvY29yZS9hcHBsaWNhdGlvbi13YWxrdGhyb3VnaC9hcHBsaWNhdGlvbi13YWxrdGhyb3VnaC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ0NKO0FEQUk7RUFDSSxZQUFBO0FDRVI7QURBSTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ0VSIiwiZmlsZSI6InNyYy9hcHAvY29yZS9hcHBsaWNhdGlvbi13YWxrdGhyb3VnaC9hcHBsaWNhdGlvbi13YWxrdGhyb3VnaC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGVudC1jb250YWluZXIge1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAganVzdGlmeS1pdGVtczogY2VudGVyO1xuICAgIHdpZHRoOiAxMDB2aDtcbiAgICBoZWlnaHQ6IDgwdmg7XG4gICAgaW1nIHtcbiAgICAgICAgd2lkdGg6IDE2cmVtO1xuICAgIH1cbiAgICBwIHtcbiAgICAgICAgbWFyZ2luLXRvcDogMXJlbTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAzcmVtO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAzcmVtXG4gICAgfVxufVxuXG4iLCIuY29udGVudC1jb250YWluZXIge1xuICBkaXNwbGF5OiBncmlkO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBqdXN0aWZ5LWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDB2aDtcbiAgaGVpZ2h0OiA4MHZoO1xufVxuLmNvbnRlbnQtY29udGFpbmVyIGltZyB7XG4gIHdpZHRoOiAxNnJlbTtcbn1cbi5jb250ZW50LWNvbnRhaW5lciBwIHtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgcGFkZGluZy1sZWZ0OiAzcmVtO1xuICBwYWRkaW5nLXJpZ2h0OiAzcmVtO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/core/application-walkthrough/application-walkthrough.page.ts":
  /*!******************************************************************************!*\
    !*** ./src/app/core/application-walkthrough/application-walkthrough.page.ts ***!
    \******************************************************************************/

  /*! exports provided: ApplicationWalkthroughPage */

  /***/
  function srcAppCoreApplicationWalkthroughApplicationWalkthroughPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ApplicationWalkthroughPage", function () {
      return ApplicationWalkthroughPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var ApplicationWalkthroughPage = /*#__PURE__*/function () {
      function ApplicationWalkthroughPage(router) {
        _classCallCheck(this, ApplicationWalkthroughPage);

        this.router = router; // Slider

        this.slidesOptions = {
          initialSlide: 0,
          speed: 400
        }; // Data

        this.pages = [{
          title: 'Reusable bag',
          text: 'Use reusable bag to reduce the usage of plastic bag',
          path: 'assets/img/ecocon/10-bag.svg'
        }, {
          title: 'Reusable paper',
          text: 'Use reusable paper to reduce trees exploitation',
          path: 'assets/img/ecocon/26-paper-reuse.svg'
        }, {
          title: 'Electric car',
          text: 'Use electric car to reduce air pollution',
          path: 'assets/img/ecocon/17-electric-car.svg'
        }];
      }

      _createClass(ApplicationWalkthroughPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "navigatePage",
        value: function navigatePage(path) {
          this.router.navigate([path]);
        }
      }]);

      return ApplicationWalkthroughPage;
    }();

    ApplicationWalkthroughPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    ApplicationWalkthroughPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-application-walkthrough',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./application-walkthrough.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/application-walkthrough/application-walkthrough.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./application-walkthrough.page.scss */
      "./src/app/core/application-walkthrough/application-walkthrough.page.scss"))["default"]]
    })], ApplicationWalkthroughPage);
    /***/
  }
}]);
//# sourceMappingURL=core-application-walkthrough-application-walkthrough-module-es5.js.map