(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-my-home-my-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/my-home/my-home.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/my-home/my-home.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>MyHome</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content [fullscreen]=\"true\">\n  <ion-list mode=\"ios\" class=\"ion-no-margin list-core\">\n    <ion-list-header mode=\"ios\">\n      <h3>Registered Homes</h3>\n    </ion-list-header>\n\n    <ion-card mode=\"ios\" [hidden]=\"isMyHomesEmpty\" *ngFor=\"let house of houses\">\n      <ion-card-content class=\"ion-no-padding ion-padding-start ion-padding-end\">\n        <ion-item class=\"ion-no-padding\" lines=\"none\">\n          <ion-icon name=\"home-outline\" slot=\"start\" color=\"primary\"></ion-icon>\n          <ion-label class=\"ion-text-wrap\">\n            <h3>{{house.assessment_tax_account}}</h3>\n            <h4 *ngIf=\"house.building_type == 'CD'\">Condominium</h4>\n            <h4 *ngIf=\"house.building_type == 'FL'\">Flat</h4>\n            <h4 *ngIf=\"house.building_type == 'TO'\">Townhouse</h4>\n            <h4 *ngIf=\"house.building_type == 'TE'\">Terrace House</h4>\n            <h4 *ngIf=\"house.building_type == 'BS'\">Bungalow / Semidetached</h4>\n            <h4 *ngIf=\"house.building_type == 'AS'\">Apartment / Service Apartment</h4>\n            <h4 *ngIf=\"house.building_type == 'OT'\">Other</h4>\n            <p>{{house.address}}, {{house.postcode}} {{house.area}}, Petaling Jaya, Selangor</p>\n          </ion-label>\n          <ion-button size=\"small\" shape=\"round\" slot=\"end\">View</ion-button>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n\n    <div [hidden]=\"!isMyHomesEmpty\">\n      <img [src]=\"iconError\" class=\"error-img\">\n      <p class=\"error-text\">No home registered yet, please register using the button below</p>\n    </div>\n  </ion-list>\n\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\" (click)=\"addHome('/my-home/add/')\">\n    <ion-fab-button>\n      <ion-icon name=\"add-circle-outline\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/core/my-home/my-home-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/core/my-home/my-home-routing.module.ts ***!
  \********************************************************/
/*! exports provided: MyHomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyHomePageRoutingModule", function() { return MyHomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _my_home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-home.page */ "./src/app/core/my-home/my-home.page.ts");




const routes = [
    {
        path: '',
        component: _my_home_page__WEBPACK_IMPORTED_MODULE_3__["MyHomePage"]
    }
];
let MyHomePageRoutingModule = class MyHomePageRoutingModule {
};
MyHomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyHomePageRoutingModule);



/***/ }),

/***/ "./src/app/core/my-home/my-home.module.ts":
/*!************************************************!*\
  !*** ./src/app/core/my-home/my-home.module.ts ***!
  \************************************************/
/*! exports provided: MyHomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyHomePageModule", function() { return MyHomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _my_home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-home-routing.module */ "./src/app/core/my-home/my-home-routing.module.ts");
/* harmony import */ var _my_home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-home.page */ "./src/app/core/my-home/my-home.page.ts");







let MyHomePageModule = class MyHomePageModule {
};
MyHomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_home_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyHomePageRoutingModule"]
        ],
        declarations: [_my_home_page__WEBPACK_IMPORTED_MODULE_6__["MyHomePage"]]
    })
], MyHomePageModule);



/***/ }),

/***/ "./src/app/core/my-home/my-home.page.scss":
/*!************************************************!*\
  !*** ./src/app/core/my-home/my-home.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error-img {\n  display: grid;\n  max-width: 7rem !important;\n  margin-top: 5rem;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.error-text {\n  color: #9d9fa6;\n  display: grid;\n  font-size: 0.9rem;\n  margin-left: 4rem;\n  margin-right: 4rem;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9tb2JpbGUvc3JjL2FwcC9jb3JlL215LWhvbWUvbXktaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2NvcmUvbXktaG9tZS9teS1ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSwwQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBREVBO0VBQ0ksY0FBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvY29yZS9teS1ob21lL215LWhvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yLWltZyB7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBtYXgtd2lkdGg6IDdyZW0gIWltcG9ydGFudDtcbiAgICBtYXJnaW4tdG9wOiA1cmVtO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbn1cbiAgXG4uZXJyb3ItdGV4dCB7XG4gICAgY29sb3I6ICM5ZDlmYTY7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBmb250LXNpemU6IDAuOXJlbTtcbiAgICBtYXJnaW4tbGVmdDogNHJlbTtcbiAgICBtYXJnaW4tcmlnaHQ6IDRyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufSIsIi5lcnJvci1pbWcge1xuICBkaXNwbGF5OiBncmlkO1xuICBtYXgtd2lkdGg6IDdyZW0gIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogNXJlbTtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbn1cblxuLmVycm9yLXRleHQge1xuICBjb2xvcjogIzlkOWZhNjtcbiAgZGlzcGxheTogZ3JpZDtcbiAgZm9udC1zaXplOiAwLjlyZW07XG4gIG1hcmdpbi1sZWZ0OiA0cmVtO1xuICBtYXJnaW4tcmlnaHQ6IDRyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/core/my-home/my-home.page.ts":
/*!**********************************************!*\
  !*** ./src/app/core/my-home/my-home.page.ts ***!
  \**********************************************/
/*! exports provided: MyHomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyHomePage", function() { return MyHomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_shared_services_houses_houses_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/houses/houses.service */ "./src/app/shared/services/houses/houses.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/auth/auth.service */ "./src/app/shared/services/auth/auth.service.ts");





let MyHomePage = class MyHomePage {
    constructor(authService, houseService, router) {
        this.authService = authService;
        this.houseService = houseService;
        this.router = router;
        // Data
        this.houses = [];
        this.id_serial = 124124;
        this.created_at = '12/05/2020';
        // Image
        this.iconError = 'assets/img/icon/error-404.svg';
        // Checker
        this.isMyHomesEmpty = true;
        this.getData();
    }
    ngOnInit() {
    }
    ngOnDestroy() {
    }
    getData() {
        let fieldOwner = 'owner=' + this.authService.userID;
        if (this.authService.userID) {
            this.houseService.filter(fieldOwner).subscribe(() => {
                this.houses = this.houseService.housesFiltered;
            }, () => { }, () => {
                if (this.houses.length >= 1) {
                    this.isMyHomesEmpty = false;
                }
            });
        }
    }
    navigateDetail(house) {
    }
    addHome(path) {
        this.router.navigate([path]);
    }
};
MyHomePage.ctorParameters = () => [
    { type: src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_shared_services_houses_houses_service__WEBPACK_IMPORTED_MODULE_2__["HousesService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
MyHomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./my-home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/my-home/my-home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./my-home.page.scss */ "./src/app/core/my-home/my-home.page.scss")).default]
    })
], MyHomePage);



/***/ })

}]);
//# sourceMappingURL=core-my-home-my-home-module-es2015.js.map