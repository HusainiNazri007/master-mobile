(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["core-application-apply-application-apply-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/application-apply/application-apply.page.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/application-apply/application-apply.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\" mode=\"ios\" class=\"ion-no-border\">\n\t<ion-toolbar mode=\"ios\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button\n\t\t\t\ttext=\"\"\n\t\t\t\tdefaultHref=\"/core/home\"\n\t\t\t\trouterLink=\"/core/home\"\n\t\t\t\trouterDirection=\"back\"\n\t\t\t\ttext=\"\"\n\t\t\t>\n\t\t\t</ion-back-button>\n    </ion-buttons>\n\t<ion-title>Apply eRebate</ion-title>\n\t<!-- <ion-button\n\t\tslot=\"end\"\n\t\tcolor=\"warning\"\n\t\tsize=\"small\"\n\t\tmode=\"ios\"\n\t\tfill=\"outline\"\n\t\t(click)=\"draft()\"\n\t>\n\t\tSave as draft\n\t</ion-button> -->\n\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content>\n\t<ion-list class=\"ion-no-margin list-core\" mode=\"ios\">\n\t\t<form [formGroup]=\"applicationForm\">\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">No. of lamp in house</ion-label>\n\t\t\t\t<ion-input placeholder=\"1\" mode=\"ios\" formControlName=\"total_lamp\"></ion-input>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">No. of LED lamp in house</ion-label>\n\t\t\t\t<ion-input placeholder=\"1\" mode=\"ios\" formControlName=\"total_led\"></ion-input>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">No. of car(s)</ion-label>\n\t\t\t\t<ion-select placeholder=\"1\" mode=\"ios\" formControlName=\"vehicle_car\">\n\t\t\t\t\t<ion-select-option *ngFor=\"let vehicle of vehicleOptions\" value=\"{{vehicle.value}}\">{{vehicle.value}}</ion-select-option>\n\t\t\t\t</ion-select>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">No. of motorcycle(s)</ion-label>\n\t\t\t\t<ion-select placeholder=\"1\" mode=\"ios\" formControlName=\"vehicle_motorcycle\">\n\t\t\t\t\t<ion-select-option *ngFor=\"let vehicle of vehicleOptions\" value=\"{{vehicle.value}}\">{{vehicle.value}}</ion-select-option>\n\t\t\t\t</ion-select>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">No. of bicycle(s)</ion-label>\n\t\t\t\t<ion-select placeholder=\"1\" mode=\"ios\" formControlName=\"vehicle_bicycle\">\n\t\t\t\t\t<ion-select-option *ngFor=\"let vehicle of vehicleOptions\" value=\"{{vehicle.value}}\">{{vehicle.value}}</ion-select-option>\n\t\t\t\t</ion-select>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">No. of other vehicle(s)</ion-label>\n\t\t\t\t<ion-select placeholder=\"1\" mode=\"ios\" formControlName=\"vehicle_other\">\n\t\t\t\t\t<ion-select-option *ngFor=\"let vehicle of vehicleOptions\" value=\"{{vehicle.value}}\">{{vehicle.value}}</ion-select-option>\n\t\t\t\t</ion-select>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Electricity bill month #1</ion-label>\n\t\t\t\t<ion-datetime class=\"input-append\"  placeholder=\"March 2020\" mode=\"ios\" displayFormat=\"MMMM YYYY\" formControlName=\"electricity_bill_1_month\"></ion-datetime>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Electricity bill usage #1</ion-label>\n\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t<ion-input class=\"input-append\" placeholder=\"1\" mode=\"ios\" formControlName=\"electricity_bill_1_usage\">\n\t\t\t\t\t</ion-input>\n\t\t\t\t\t<div class=\"input-group-append\">\n\t\t\t\t\t\t<span>kWh</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item  class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Electricity bill document #1</ion-label>\n\t\t\t\t<ion-button mode=\"ios\" color=\"primary\" expand=\"block\" (click)=\"openUploadSheet(1)\">Upload</ion-button>\n\t\t\t\t<ion-img [src]=\"imageSelectedPreview1\" *ngIf=\"imageSelectedPreview1\"></ion-img>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Electricity bill month #2</ion-label>\n\t\t\t\t<ion-datetime  placeholder=\"March 2020\" mode=\"ios\" displayFormat=\"MMMM YYYY\" formControlName=\"electricity_bill_2_month\"></ion-datetime>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Electricity bill usage #2</ion-label>\n\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t<ion-input class=\"electricity input-append\" placeholder=\"1\" mode=\"ios\" formControlName=\"electricity_bill_2_usage\">\n\t\t\t\t\t</ion-input>\n\t\t\t\t\t<div class=\"input-group-append\">\n\t\t\t\t\t\t<span>kWh</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item  class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Electricity bill document #1</ion-label>\n\t\t\t\t<ion-button mode=\"ios\" color=\"primary\" expand=\"block\" (click)=\"openUploadSheet(2)\">Upload</ion-button>\n\t\t\t\t<ion-img [src]=\"imageSelectedPreview2\" *ngIf=\"imageSelectedPreview2\"></ion-img>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Electricity bill month #3</ion-label>\n\t\t\t\t<ion-datetime  placeholder=\"March 2020\" mode=\"ios\" displayFormat=\"MMMM YYYY\" formControlName=\"electricity_bill_3_month\"></ion-datetime>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Electricity bill usage #3</ion-label>\n\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t<ion-input class=\"electricity input-append\" placeholder=\"1\" mode=\"ios\" formControlName=\"electricity_bill_3_usage\">\n\t\t\t\t\t</ion-input>\n\t\t\t\t\t<div class=\"input-group-append\">\n\t\t\t\t\t\t<span>kWh</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item  class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Electricity bill document #3</ion-label>\n\t\t\t\t<ion-button mode=\"ios\" color=\"primary\" expand=\"block\" (click)=\"openUploadSheet(3)\">Upload</ion-button>\n\t\t\t\t<ion-img [src]=\"imageSelectedPreview3\" *ngIf=\"imageSelectedPreview3\"></ion-img>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Water bill month #1</ion-label>\n\t\t\t\t<ion-datetime  placeholder=\"March 2020\" mode=\"ios\" displayFormat=\"MMMM YYYY\" formControlName=\"water_bill_1_month\"></ion-datetime>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Water bill usage #1</ion-label>\n\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t<ion-input class=\"electricity input-append\" placeholder=\"1\" mode=\"ios\" formControlName=\"water_bill_1_usage\">\n\t\t\t\t\t</ion-input>\n\t\t\t\t\t<div class=\"input-group-append\">\n\t\t\t\t\t\t<span>m<sup>3</sup></span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item  class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Water bill document #1</ion-label>\n\t\t\t\t<ion-button mode=\"ios\" color=\"primary\" expand=\"block\" (click)=\"openUploadSheet(4)\">Upload</ion-button>\n\t\t\t\t<ion-img [src]=\"imageSelectedPreview4\" *ngIf=\"imageSelectedPreview4\"></ion-img>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Water bill month #2</ion-label>\n\t\t\t\t<ion-datetime  placeholder=\"March 2020\" mode=\"ios\" displayFormat=\"MMMM YYYY\" formControlName=\"water_bill_2_month\"></ion-datetime>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Water bill usage #2</ion-label>\n\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t<ion-input class=\"electricity input-append\" placeholder=\"1\" mode=\"ios\" formControlName=\"water_bill_2_usage\">\n\t\t\t\t\t</ion-input>\n\t\t\t\t\t<div class=\"input-group-append\">\n\t\t\t\t\t\t<span>m<sup>3</sup></span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item  class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Water bill document #2</ion-label>\n\t\t\t\t<ion-button mode=\"ios\" color=\"primary\" expand=\"block\" (click)=\"openUploadSheet(5)\">Upload</ion-button>\n\t\t\t\t<ion-img [src]=\"imageSelectedPreview5\" *ngIf=\"imageSelectedPreview5\"></ion-img>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Water bill month #3</ion-label>\n\t\t\t\t<ion-datetime  placeholder=\"March 2020\" mode=\"ios\" displayFormat=\"MMMM YYYY\" formControlName=\"water_bill_3_month\"></ion-datetime>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Water bill usage #3</ion-label>\n\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t<ion-input class=\"electricity input-append\" placeholder=\"1\" mode=\"ios\" formControlName=\"water_bill_3_usage\">\n\t\t\t\t\t</ion-input>\n\t\t\t\t\t<div class=\"input-group-append\">\n\t\t\t\t\t\t<span>m<sup>3</sup></span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item  class=\"item-form\" lines=\"none\">\n\t\t\t\t<ion-label position=\"stacked\" mode=\"ios\">Water bill document #3</ion-label>\n\t\t\t\t<ion-button mode=\"ios\" color=\"primary\" expand=\"block\" (click)=\"openUploadSheet(6)\">Upload</ion-button>\n\t\t\t\t<ion-img [src]=\"imageSelectedPreview6\" *ngIf=\"imageSelectedPreview6\"></ion-img>\n\t\t\t</ion-item>\n\t\t</form>\n\t</ion-list>\n</ion-content>\n\n<ion-footer class=\"ion-no-border ion-padding\" mode=\"ios\">\n\t<ion-button \n\t\t*ngIf=\"!isLoading\" \n\t\texpand=\"block\" \n\t\tmode=\"ios\" \n\t\tshape=\"round\" \n\t\tcolor=\"primary\" \n\t\ttype=\"submit\"\n\t\t(click)=\"submit()\"\n\t>\n\t\tSubmit\n\t</ion-button>\n\t<ion-button \n\t\t*ngIf=\"isLoading\" \n\t\texpand=\"block\" \n\t\tmode=\"ios\"\n\t\tshape=\"round\"\n\t\tcolor=\"primary\"\n\t>\n\t\t<ion-spinner name=\"bubbles\"></ion-spinner>\n\t</ion-button>\n</ion-footer>");

/***/ }),

/***/ "./src/app/core/application-apply/application-apply-routing.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/core/application-apply/application-apply-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: ApplicationApplyPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationApplyPageRoutingModule", function() { return ApplicationApplyPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _application_apply_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./application-apply.page */ "./src/app/core/application-apply/application-apply.page.ts");




const routes = [
    {
        path: '',
        component: _application_apply_page__WEBPACK_IMPORTED_MODULE_3__["ApplicationApplyPage"]
    }
];
let ApplicationApplyPageRoutingModule = class ApplicationApplyPageRoutingModule {
};
ApplicationApplyPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ApplicationApplyPageRoutingModule);



/***/ }),

/***/ "./src/app/core/application-apply/application-apply.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/core/application-apply/application-apply.module.ts ***!
  \********************************************************************/
/*! exports provided: ApplicationApplyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationApplyPageModule", function() { return ApplicationApplyPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _application_apply_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./application-apply-routing.module */ "./src/app/core/application-apply/application-apply-routing.module.ts");
/* harmony import */ var _application_apply_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./application-apply.page */ "./src/app/core/application-apply/application-apply.page.ts");







let ApplicationApplyPageModule = class ApplicationApplyPageModule {
};
ApplicationApplyPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _application_apply_routing_module__WEBPACK_IMPORTED_MODULE_5__["ApplicationApplyPageRoutingModule"]
        ],
        declarations: [_application_apply_page__WEBPACK_IMPORTED_MODULE_6__["ApplicationApplyPage"]]
    })
], ApplicationApplyPageModule);



/***/ }),

/***/ "./src/app/core/application-apply/application-apply.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/core/application-apply/application-apply.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".construction-text {\n  color: #9d9fa6;\n  font-size: 0.9rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zeWFmaXFiYXNyaS9EZXZlbG9wbWVudC9Qcm9qZWN0L1NJQzAwMS9tb2JpbGUvc3JjL2FwcC9jb3JlL2FwcGxpY2F0aW9uLWFwcGx5L2FwcGxpY2F0aW9uLWFwcGx5LnBhZ2Uuc2NzcyIsInNyYy9hcHAvY29yZS9hcHBsaWNhdGlvbi1hcHBseS9hcHBsaWNhdGlvbi1hcHBseS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvYXBwbGljYXRpb24tYXBwbHkvYXBwbGljYXRpb24tYXBwbHkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnN0cnVjdGlvbi10ZXh0IHtcbiAgICBjb2xvcjogIzlkOWZhNjtcbiAgICBmb250LXNpemU6IDAuOXJlbTtcbn1cbiIsIi5jb25zdHJ1Y3Rpb24tdGV4dCB7XG4gIGNvbG9yOiAjOWQ5ZmE2O1xuICBmb250LXNpemU6IDAuOXJlbTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/core/application-apply/application-apply.page.ts":
/*!******************************************************************!*\
  !*** ./src/app/core/application-apply/application-apply.page.ts ***!
  \******************************************************************/
/*! exports provided: ApplicationApplyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationApplyPage", function() { return ApplicationApplyPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_shared_services_applications_applications_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/applications/applications.service */ "./src/app/shared/services/applications/applications.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/auth/auth.service */ "./src/app/shared/services/auth/auth.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_shared_handlers_notify_notify_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/handlers/notify/notify.service */ "./src/app/shared/handlers/notify/notify.service.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var src_assets_data_vehicles__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/assets/data/vehicles */ "./src/assets/data/vehicles.ts");
/* harmony import */ var _tips_tips_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../tips/tips.page */ "./src/app/core/tips/tips.page.ts");











let ApplicationApplyPage = class ApplicationApplyPage {
    constructor(authService, applicationService, actionSheetCtrl, camera, fb, popoverCtrl, toastr, router) {
        this.authService = authService;
        this.applicationService = applicationService;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.fb = fb;
        this.popoverCtrl = popoverCtrl;
        this.toastr = toastr;
        this.router = router;
        // Image
        this.imgConstruction = 'assets/img/default/Construction.png';
        // Loading
        this.isLoading = false;
        // Type
        this.vehicleOptions = src_assets_data_vehicles__WEBPACK_IMPORTED_MODULE_9__["Vehicles"];
    }
    ngOnInit() {
        this.applicationForm = this.fb.group({
            applicant: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            applied_house: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            total_lamp: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](1, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            total_led: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            vehicle_car: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            vehicle_motorcycle: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            vehicle_bicycle: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            vehicle_other: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            electricity_bill_1_month: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            electricity_bill_1_usage: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            electricity_bill_1_doc: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            electricity_bill_2_month: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            electricity_bill_2_usage: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            electricity_bill_2_doc: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            electricity_bill_3_month: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            electricity_bill_3_usage: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            electricity_bill_3_doc: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            water_bill_1_month: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            water_bill_1_usage: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            water_bill_1_doc: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            water_bill_2_month: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            water_bill_2_usage: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            water_bill_2_doc: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            water_bill_3_month: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            water_bill_3_usage: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](0, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ])),
            water_bill_3_doc: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required
            ]))
        });
        this.applicationForm.controls['applicant'].setValue(this.authService.userID);
    }
    confirm() {
    }
    submit() {
        // this.isLoading = true
        // this.applicationService.create(this.applicationForm.value).subscribe(
        //   () => {},
        //   () => {},
        //   () => {}
        // )
        this.toastr.openToastr('Your application has been submitted!');
        this.navigatePage('/core/home');
    }
    draft() {
    }
    openUploadSheet(index) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetCtrl.create({
                header: '',
                // cssClass: 'my-custom-class',
                buttons: [
                    {
                        text: 'Gallery',
                        icon: 'images-outline',
                        handler: () => {
                            this.openGallery(index);
                        }
                    },
                    {
                        text: 'Camera',
                        icon: 'camera-outline',
                        handler: () => {
                            this.openCamera(index);
                        }
                    },
                    {
                        text: 'Cancel',
                        icon: 'close-outline',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    openGallery(index) {
        // console.log('Gallery opened')
        let cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.FILE_URI,
            quality: 60,
            targetWidth: 1000,
            targetHeight: 1000,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
        };
        this.camera.getPicture(cameraOptions)
            .then((file_uri) => {
            if (index == 1) {
                // Electricity bill 1
                this.imageSelected1 = file_uri;
                this.imageSelectedPreview1 = window.Ionic.WebView.convertFileSrc(this.imageSelected1);
                this.applicationForm.controls['electricity_bill_1_doc'].setValue(this.imageSelected1);
            }
            else if (index == 2) {
                // Electricity bill 2
                this.imageSelected2 = file_uri;
                this.imageSelectedPreview2 = window.Ionic.WebView.convertFileSrc(this.imageSelected2);
                this.applicationForm.controls['electricity_bill_2_doc'].setValue(this.imageSelected2);
            }
            else if (index == 3) {
                // Electricity bill 3
                this.imageSelected3 = file_uri;
                this.imageSelectedPreview3 = window.Ionic.WebView.convertFileSrc(this.imageSelected3);
                this.applicationForm.controls['electricity_bill_3_doc'].setValue(this.imageSelected3);
            }
            else if (index == 4) {
                // Water bill 1
                this.imageSelected4 = file_uri;
                this.imageSelectedPreview4 = window.Ionic.WebView.convertFileSrc(this.imageSelected4);
                this.applicationForm.controls['water_bill_1_doc'].setValue(this.imageSelected4);
            }
            else if (index == 5) {
                // Water bill 2
                this.imageSelected5 = file_uri;
                this.imageSelectedPreview5 = window.Ionic.WebView.convertFileSrc(this.imageSelected5);
                this.applicationForm.controls['water_bill_2_doc'].setValue(this.imageSelected5);
            }
            else if (index == 6) {
                // Water bill 3
                this.imageSelected6 = file_uri;
                this.imageSelectedPreview6 = window.Ionic.WebView.convertFileSrc(this.imageSelected6);
                this.applicationForm.controls['water_bill_3_doc'].setValue(this.imageSelected6);
            }
        }, (err) => {
            console.log(err);
        });
    }
    openCamera(index) {
        // console.log('Camera opened')
        const options = {
            quality: 60,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.camera.getPicture(options).then((imageData) => {
            if (index == 1) {
                // Electricity bill 1
                this.imageSelected1 = imageData;
                this.imageSelectedPreview1 = window.Ionic.WebView.convertFileSrc(this.imageSelected1);
                this.applicationForm.controls['electricity_bill_1_doc'].setValue(this.imageSelected1);
            }
            else if (index == 2) {
                // Electricity bill 2
                this.imageSelected2 = imageData;
                this.imageSelectedPreview2 = window.Ionic.WebView.convertFileSrc(this.imageSelected2);
                this.applicationForm.controls['electricity_bill_2_doc'].setValue(this.imageSelected2);
            }
            else if (index == 3) {
                // Electricity bill 3
                this.imageSelected3 = imageData;
                this.imageSelectedPreview3 = window.Ionic.WebView.convertFileSrc(this.imageSelected3);
                this.applicationForm.controls['electricity_bill_3_doc'].setValue(this.imageSelected3);
            }
            else if (index == 4) {
                // Water bill 1
                this.imageSelected4 = imageData;
                this.imageSelectedPreview4 = window.Ionic.WebView.convertFileSrc(this.imageSelected4);
                this.applicationForm.controls['water_bill_1_doc'].setValue(this.imageSelected4);
            }
            else if (index == 5) {
                // Water bill 2
                this.imageSelected5 = imageData;
                this.imageSelectedPreview5 = window.Ionic.WebView.convertFileSrc(this.imageSelected5);
                this.applicationForm.controls['water_bill_2_doc'].setValue(this.imageSelected5);
            }
            else if (index == 6) {
                // Water bill 3
                this.imageSelected6 = imageData;
                this.imageSelectedPreview6 = window.Ionic.WebView.convertFileSrc(this.imageSelected6);
                this.applicationForm.controls['water_bill_3_doc'].setValue(this.imageSelected6);
            }
        }, (err) => {
            alert("error " + JSON.stringify(err));
        });
    }
    navigatePage(path) {
        this.router.navigate([path]);
    }
    viewTips(type) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _tips_tips_page__WEBPACK_IMPORTED_MODULE_10__["TipsPage"],
                // cssClass: 'my-custom-class',
                componentProps: {
                    'type': type
                },
                translucent: true
            });
            return yield popover.present();
        });
    }
};
ApplicationApplyPage.ctorParameters = () => [
    { type: src_app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_shared_services_applications_applications_service__WEBPACK_IMPORTED_MODULE_2__["ApplicationsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ActionSheetController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_7__["Camera"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["PopoverController"] },
    { type: src_app_shared_handlers_notify_notify_service__WEBPACK_IMPORTED_MODULE_6__["NotifyService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] }
];
ApplicationApplyPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-application-apply',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./application-apply.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/core/application-apply/application-apply.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./application-apply.page.scss */ "./src/app/core/application-apply/application-apply.page.scss")).default]
    })
], ApplicationApplyPage);



/***/ }),

/***/ "./src/assets/data/vehicles.ts":
/*!*************************************!*\
  !*** ./src/assets/data/vehicles.ts ***!
  \*************************************/
/*! exports provided: Vehicles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Vehicles", function() { return Vehicles; });
const Vehicles = [
    { value: 1 },
    { value: 2 },
    { value: 3 },
    { value: 4 },
    { value: 5 },
    { value: 6 },
    { value: 7 },
    { value: 8 },
    { value: 9 },
    { value: 10 }
];


/***/ })

}]);
//# sourceMappingURL=core-application-apply-application-apply-module-es2015.js.map